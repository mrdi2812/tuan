﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace KTV.Model
{
    [Table("PhieuChis")]
    public class PhieuChi
    {
        [Key]
        public string MaPhieuChi { set; get; }

        public DateTime NgayThu { set; get; }

        public string NguoiThu { set; get; }

        public double TongTien { set; get; }

        public string PTThanhToan { set; get; }

        public string HangMucChi { set; get; }

        public string GhiChu { set; get; }

        public int? KhoId { set; get; }

        public int? NVTN { set; get; }

        public int? NCCId { set; get; }
    }
}
