﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace KTV.Model
{
    [Table("HangHoas")]
    public class HangHoa
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { set; get; }

        public string RootId { set; get; }
        [StringLength(25)]
        [Column(TypeName = "varchar")]
        public string MaHang { set; get; }

        [StringLength(300)]
        public string TenHang { set; get;}

        public double SoLuong { set; get; }

        public int NhomHangId { set; get; }

        [StringLength(250)]
        public string NhaSanXuat { set; get; }

        public double GiaMua { set; get; }

        public double GiaBan { set; get; }

        public int VAT { set; get; }

        public bool CheckVATMua { set; get; }

        public bool CheckVATBan { set; get; }

        public bool Status { set; get; }

        public bool CheckHome { set; get; }

        public string GhiChu { set; get; }

        public string ThumbnalImage { set; get; }

        [StringLength(50)]
        public string XuatXu { set; get; }

        [StringLength(50)]
        public string DonViTinh { set; get; }
    }
}
