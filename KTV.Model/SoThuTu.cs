﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace KTV.Model
{
    [Table("SoThuTus")]
    public class SoThuTu
    {
        [Key]
        public string Name { get; set; }

        public string Pre { get; set; }

        public int Nu { get; set; }

        public int Stt { get; set; }

        public string Sub { get; set; }
    }
}
