﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace KTV.Model
{
    [Table("CtTiens")]
    public class CtTien
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { set; get; }

        public string MaHDId { set; get; }

        public string LoaiHD { set; get; }

        public DateTime NgayHoaDon { set; get; }

        public int? TKId { set; get; }

        public int? TKDoiUngId { set; get; }

        public double PsNo { set; get; }

        public double PsCo { set; get; }

    }
}
