﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace KTV.Model
{
    [Table("ThuocTinhGroups")]
    public class ThuocTinhGroup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }
        public string RootHHId { set; get; }

        public int ThuocTinhId { set; get; }

        public string Values { set; get; }
    }
}
