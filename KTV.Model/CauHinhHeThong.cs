﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace KTV.Model
{
    [Table("CauHinhHeThongs")]
    public class CauHinhHeThong
    {
        [Key]
        [StringLength(250)]
        public string Name { get; set; }

        [Required]
        public string Descript { get; set; }

        [Required]
        public string Value { get; set; }

        [Required]
        public string Default { get; set; }

        public string User_Create { get; set; }

        public string User_Edit { get; set; }

        public DateTime? Datetime_Create { get; set; }

        public DateTime? Datetime_Edit { get; set; }
    }
}
