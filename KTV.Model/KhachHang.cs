﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace KTV.Model
{
    [Table("KhachHangs")]
    public class KhachHang
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        [Required]
        [StringLength(25)]
        public string Ma { set; get; }

        [Required(ErrorMessage = "{0} Không được bỏ trống!")]
        public string Name { set; get; }

        [StringLength(15)]
        public string Phone { set; get; }

        [StringLength(300)]
        public string DiaChi { set; get; }

        public string Email { set; get; }

        public DateTime? NgaySinh { set; get; }

        public string GioiTinh { set; get; }

        public string GhiChu { set; get; }

        public bool Status { set; get; }

        [StringLength(50)]
        public string MaSoThue { set; get; }

        [StringLength(200)]
        public string NguoiLienHe { set; get; }

    }
}
