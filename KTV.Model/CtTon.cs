﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace KTV.Model
{
    [Table("CtTons")]
    public class CtTon
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { set; get; }

        public string MaHDId { set; get; }

        public string LoaiHD { set; get; }

        public DateTime NgayHoaDon { set; get; }

        public int? KhoId { set; get; }

        public int? KhoDoiUngId { set; get; }

        public double SoLuongNhap { set; get; }

        public double SoLuongXuat { set; get; }

        public string MaHangId { set; get; }



    }
}
