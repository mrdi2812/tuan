﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace KTV.Model
{
    [Table("ThongTins")]
    public class ThongTin
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { set; get; }

        [StringLength(250)]
        public string Name { set; get; }

        [StringLength(500)]
        public string Address { set; get; }

        [StringLength(15)]
        public string Phone { set; get; }

        [StringLength(250)]
        public string GhiChu { set; get; }

    }
}
