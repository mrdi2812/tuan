﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace KTV.Model
{
    [Table("CtBanHangs")]
    public class CtBanHang
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }
        public string MaHD { get; set; }

        public string MaHang { set; get; }

        public double SoLuong { set; get; }

        public double GiaVon { set; get; }

        public double GiaBan { set; get; }

        public double CK { set; get; }

        public double CK_Value { set; get; }

        public double ThanhTien { set; get; }

        public string GhiChu { set; get; }

    }
}
