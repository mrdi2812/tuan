﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace KTV.Model
{
    [Table("BanHangs")]
    public class BanHang
    {
        [Key]
        public string MaHD { set; get; }

        public string LoaiHD { set; get; }

        public DateTime NgayBan { set; get; }

        public int? NVBH { set; get; }
        
        public int? NVTN { set; get; }

        public int? NVKT { set; get; }

        public int? NVKho { set; get; }

        public int? KhachHangId { set; get; }

        public int Status { set; get; }

        public double TongTien { set; get; }

        public double TongSL { set; get; }

        public string SoGiaoDich { set; get; }

        public int? KhoId { set; get; }

        public string GhiChu { set; get; }

        public double KhuyenMai { set; get; }

        public double KhachDua_Tong { set; get; }

        public double KhachDua_TienMat { set; get; }

        public double KhachDua_The { set; get; }

        public double KhachDua_ChuyenKhoan { set; get; }

        public double Congno { set; get; }

        public double Tralai { set; get; }
    }
}
