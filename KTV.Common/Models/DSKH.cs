﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Common.Models
{
    public class DSKH
    {
        public int Id { set; get; }

        public string Ma { set; get; }

        public string Name { set; get; }

        public string Phone { set; get; }

        public DateTime NgayMua { set; get; }

        public double TongTien { set; get; }

        public double TongNo { set; get; }

        public double TienTraHang { set; get; }
    }
}
