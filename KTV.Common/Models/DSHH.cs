﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Common.Models
{
    public class DSHH
    {
        public string MaHang { set; get; }
        public string TenHang { set; get; }
        public double SoLuong { set; get; }
        public DateTime NgayMua { set; get; }
        public double GiaBan { set; get; }
    }
}
