﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.App
{
    public class HangHoaVM
    {

        public string MaHang { set; get; }

        public string TenHang { set; get; }

        public double SoLuong { set; get; }

        public double GiaMua { set; get; }

        public double GiaBan { set; get; }

    }
}
