﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace KTV.App.Template
{
    public partial class TemKinh : DevExpress.XtraReports.UI.XtraReport
    {
        public string TenCH { set; get; }
        public string DiaChi { set; get; }
        public string GhiChu { set; get; }

        public TemKinh()
        {
            InitializeComponent();
        }
        public TemKinh(string _TenCH, string _DiaChi,string _GhiChu) : this()
        {
            lblTenCH.Text = _TenCH;
            lblDiaChi.Text = _DiaChi;
            lblGhiChu.Text = _GhiChu;
        }
    }
}
