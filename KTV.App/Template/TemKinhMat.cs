﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace KTV.App.Template
{
    public partial class TemKinhMat : DevExpress.XtraReports.UI.XtraReport
    {
        public TemKinhMat()
        {
            InitializeComponent();
        }
        public TemKinhMat(string _TenCH, string _DiaChi, string _GhiChu) : this()
        {
            lblTenCH.Text = _TenCH;
            lblDiaChi.Text = _DiaChi;
            lblGhiChu.Text = _GhiChu;
        }
    }
}
