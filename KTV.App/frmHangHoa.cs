﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KTV.Service;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using KTV.Common;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars;
using KTV.Model;
using DevExpress.Export.Xl;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using Excel = Microsoft.Office.Interop.Excel;
using KTV.Data;
using System.Net;

namespace KTV.App
{
    public partial class frmHangHoa : DevExpress.XtraEditors.XtraForm
    {
        HangHoaService hangHoaService = new HangHoaService();
        NhomHHService nhomHHService = new NhomHHService();
        ThuocTinhGroupService ttGroupService = new ThuocTinhGroupService();
        ThuocTinhService thuocTinhService = new ThuocTinhService();
        CtBanHangService ctBanHangService = new CtBanHangService();
        NhaSXService nhaSXService = new NhaSXService();
        int index;
        int index1;
        Guid maGiuld;
        string IPMAYCHU;
        DataTable dt1, dt2;
        List<DanhSach> list1, list2, list3, list4;
        string gt1, gt2, gt3, gt4;
        public frmHangHoa()
        {
            InitializeComponent();
            dt1 = new DataTable();
            dt2 = new DataTable();
            var listmh = nhomHHService.GetAll();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[2] { new DataColumn("ID"), new DataColumn("Ten") });
            foreach (var item in listmh)
            {
                dt.Rows.Add(item.Id, item.Name);
            }
            cbNhomHang.DataSource = dt;
            cbNhomHang.ValueMember = "ID";
            cbNhomHang.DisplayMember = "Ten";
            var listnsx = nhaSXService.GetAll();
            DataTable dt3 = new DataTable();
            dt3.Columns.AddRange(new DataColumn[2] { new DataColumn("ID"), new DataColumn("Ten") });
            foreach (var item in listmh)
            {
                dt.Rows.Add(item.Name, item.Name);
            }
            cbNSX.DataSource = dt;
            cbNSX.ValueMember = "ID";
            cbNSX.DisplayMember = "Ten";
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmEditHangHoa child = new frmEditHangHoa();
            child.FormClosed += Child_FormClosed;
            child.ShowDialog();
        }

        private void Child_FormClosed(object sender, FormClosedEventArgs e)
        {
            GetData();
        }

        private void frmHangHoa_Load(object sender, EventArgs e)
        {
            grControl.DataBindings.Clear();
            dt1.Clear();
            dt2.Clear();
            GetData();
        }
        public void GetData()
        {          
            IPHostEntry Ip = Dns.GetHostByName(Dns.GetHostName());
            foreach (IPAddress ip in Ip.AddressList)
            {
                IPMAYCHU = ip.ToString();
            }
            list1 = new List<DanhSach>();
            list2 = new List<DanhSach>();
            list3 = new List<DanhSach>();
            list4 = new List<DanhSach>();
            DataSet dbSet = new DataSet();
            gt1 = "";
            gt2 = "";
            gt3 = "";
            gt4 = "";
            System.Data.DataTable dt1 = SinhMa.ToDataTable(hangHoaService.GetAllParent());
            dt1.TableName = "Parents";
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                if (dt1.Rows[i]["MaHang"].ToString() == "")
                {
                    dt1.Rows[i]["MaHang"] = "Hàng nhiều loại";
                }

                if (dt1.Rows[i]["RootId"].ToString() == "")
                {
                    dt1.Rows[i]["RootId"] = i;
                }
                if (dt1.Rows[i]["ThumbnalImage"].ToString() != "")
                {
                    string chuoi ="\\\\"+IPMAYCHU+"\\D$\\Images\\"+dt1.Rows[i]["ThumbnalImage"].ToString();
                    dt1.Rows[i]["ThumbnalImage"] = chuoi;
                }
            }
            System.Data.DataTable dt2 = SinhMa.ToDataTable(hangHoaService.GetAllChild());
            dt2.TableName = "Childs";
            dt2.Columns["RootId"].ColumnName = "ChildId";
            dt2.Columns.Remove("CheckHome");
            dt2.Columns.Remove("CheckVATBan");
            dt2.Columns.Remove("CheckVATMua");
            dt2.Columns.Remove("VAT");
            dt2.Columns.Remove("ThumbnalImage");
            dbSet.Tables.Add(dt1);
            dbSet.Tables.Add(dt2);
            DataRelation relation = new DataRelation("Hàng cùng loại",
            dt1.Columns["RootId"],
            dt2.Columns["ChildId"]);
            dbSet.Relations.Add(relation);
            grControl.DataSource = dbSet.Tables["Parents"];
            grControl.ForceInitialize();
            //RepositoryItemGraphicsEdit repItemGraphicsEdit = new RepositoryItemGraphicsEdit();
            //repItemGraphicsEdit.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            //grView.Columns["ThumbnalImage"].ColumnEdit = repItemGraphicsEdit;
            grControl.LevelTree.Nodes.Add("Hàng cùng loại", grViewChild);         
        }
        private void grView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            index = e.FocusedRowHandle;
        }
        private void grViewChild_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            index1 = e.FocusedRowHandle;
        }
        private void btnEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (double.Parse(grViewChild.GetRowCellValue(index1, grViewChild.Columns["GiaBan"]).ToString()) > double.Parse(grViewChild.GetRowCellValue(index1, grViewChild.Columns["GiaMua"]).ToString()))
            {
                var model = hangHoaService.GetById(int.Parse(grViewChild.GetRowCellValue(index1, grViewChild.Columns["ID"]).ToString()));
                model.GiaBan = double.Parse(grViewChild.GetRowCellValue(index1, grViewChild.Columns["GiaBan"]).ToString());
                hangHoaService.Update(model);
                hangHoaService.Save();
                XtraMessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            else
            {
                MessageBox.Show("Giá bán không được nhỏ hơn giá mua");
            }
        }
        private void btnEditHH_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                if (double.Parse(grView.GetRowCellValue(index, grView.Columns[3]).ToString()) > double.Parse(grView.GetRowCellValue(index, grView.Columns["GiaMua"]).ToString()))
                {
                    var model = hangHoaService.GetById(int.Parse(grView.GetRowCellValue(index, grView.Columns["ID"]).ToString()));
                    model.GiaBan = double.Parse(grView.GetRowCellValue(index, grView.Columns[3]).ToString());
                    hangHoaService.Update(model);
                    hangHoaService.Save();
                    XtraMessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                }
                else
                {
                    MessageBox.Show("Giá bán không được nhỏ hơn giá mua");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void grView_MasterRowExpanded(object sender, CustomMasterRowEventArgs e)
        {
            grViewChild = (DevExpress.XtraGrid.Views.Grid.GridView)grView.GetDetailView(e.RowHandle, e.RelationIndex);
            grViewChild.MoveLast(); //This was asked so we go to the last revision.
            grViewChild.UnselectRow(grView.FocusedRowHandle); // This is the MasterRow
            grViewChild.SelectRow(grViewChild.FocusedRowHandle); // This is the DetailRow
        }

        private void btnThaoTac_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            barManager1.ForceInitialize();
            DevExpress.XtraBars.PopupMenu menu = new DevExpress.XtraBars.PopupMenu();
            menu.Manager = barManager1;
            //barManager1.Images = imageCollection1;
            BarButtonItem itemCopy = new BarButtonItem(barManager1, "Chỉnh sửa", 0);
            itemCopy.ItemClick += ItemCopy_ItemClick;
            BarButtonItem itemPaste = new BarButtonItem(barManager1, "Copy", 1);
            itemPaste.ItemClick += ItemPaste_ItemClick;
            BarButtonItem itemRefresh = new BarButtonItem(barManager1, "Ngừng kinh doanh", 2);
            itemRefresh.ItemClick += ItemRefresh_ItemClick;
            BarButtonItem itemDelete = new BarButtonItem(barManager1, "Xóa", 3);
            itemDelete.ItemClick += ItemDelete_ItemClick;
            menu.AddItems(new BarItem[] { itemCopy, itemPaste, itemRefresh, itemDelete });
            // Create a separator before the Refresh item.
            itemRefresh.Links[0].BeginGroup = true;
            // Process item clicks.
            // Associate the popup menu with the form.
            barManager1.SetPopupContextMenu(this, menu);
            menu.ShowPopup(Control.MousePosition);
        }
        private void ItemDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogResult dr;
            dr = XtraMessageBox.Show("Bạn muốn xóa sản phẩm này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                try
                {
                    var model = hangHoaService.GetById(int.Parse(grView.GetRowCellValue(index, grView.Columns["ID"]).ToString()));
                    if (model.RootId != null)
                    {
                        var listDB = hangHoaService.GetListByRootId(model.RootId);
                        foreach (var item in listDB)
                        {
                            hangHoaService.Delete(item.ID);
                        }
                    }
                    hangHoaService.Delete(model.ID);
                    grControl.DataSource = null;
                    DataSet dbSet = new DataSet();
                    System.Data.DataTable dt1 = SinhMa.ToDataTable(hangHoaService.GetAllParent());
                    dt1.TableName = "Parents";
                    dt1.Columns.Add("NhomHang");
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        if (dt1.Rows[i]["MaHang"].ToString() == "")
                        {
                            dt1.Rows[i]["MaHang"] = "Hàng nhiều loại";
                        }

                        if (dt1.Rows[i]["RootId"].ToString() == "")
                        {
                            dt1.Rows[i]["RootId"] = i;
                        }
                    }
                    System.Data.DataTable dt2 = SinhMa.ToDataTable(hangHoaService.GetAllChild());
                    dt2.TableName = "Childs";
                    dt2.Columns["RootId"].ColumnName = "ChildId";
                    dt2.Columns.Remove("CheckHome");
                    dt2.Columns.Remove("CheckVATBan");
                    dt2.Columns.Remove("CheckVATMua");
                    dt2.Columns.Remove("VAT");
                    dt2.Columns.Remove("ThumbnalImage");
                    dbSet.Tables.Add(dt1);
                    dbSet.Tables.Add(dt2);
                    DataRelation relation = new DataRelation("Hàng cùng loại",
                    dt1.Columns["RootId"],
                    dt2.Columns["ChildId"]);
                    dbSet.Relations.Add(relation);
                    grControl.DataBindings.Clear();
                    grControl.DataSource = dbSet.Tables["Parents"];
                    grControl.ForceInitialize();
                    grControl.LevelTree.Nodes.Add("Hàng cùng loại", grViewChild);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private void ItemRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogResult dr;
            dr = XtraMessageBox.Show("Bạn muốn ngừng kinh doanh sản phẩm này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                try
                {
                    var model = hangHoaService.GetById(int.Parse(grView.GetRowCellValue(index, grView.Columns["ID"]).ToString()));
                    if (model.RootId != null)
                    {
                        var listDB = hangHoaService.GetListByRootId(model.RootId);
                        foreach (var item in listDB)
                        {
                            item.Status = false;
                            hangHoaService.Update(item);
                        }
                    }
                    model.Status = false;
                    hangHoaService.Update(model);
                }
                catch (Exception)
                {

                    throw;
                }
            }

        }

        private void ItemPaste_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmEditHangHoa hh = new frmEditHangHoa();
            hh.Thaotac = "Copy";
            hh.ID = int.Parse(grView.GetRowCellValue(index, "ID").ToString());
            hh.FormClosed += new FormClosedEventHandler(Child_FormClosed);
            hh.ShowDialog();
        }

        private void ItemCopy_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmEditHangHoa child = new frmEditHangHoa();
            child.Thaotac = "Edit";
            child.ID = int.Parse(grView.GetRowCellValue(index, "ID").ToString());
            child.FormClosed += new FormClosedEventHandler(Child_FormClosed);
            child.ShowDialog();
        }

        private void btnKho_Click(object sender, EventArgs e)
        {
            if(grView.GetRowCellValue(index, "MaHang").ToString()=="Hàng nhiều loại")
            {

            }
            else
            {
              
            }
        }

        private void btnInMaVach_Click(object sender, EventArgs e)
        {
            frmInMaVach child = new frmInMaVach();
            child.ShowDialog();
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            grControl.DataBindings.Clear();
            dt1.Clear();
            dt2.Clear();
            GetData();
        }

        private void btnThaotac1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            barManager1.ForceInitialize();
            DevExpress.XtraBars.PopupMenu menu = new DevExpress.XtraBars.PopupMenu();
            menu.Manager = barManager1;
            //barManager1.Images = imageCollection1;
            BarButtonItem itemRefresh1 = new BarButtonItem(barManager1, "Ngừng kinh doanh", 0);
            itemRefresh1.ItemClick += ItemRefresh1_ItemClick;
            menu.AddItems(new BarItem[] { itemRefresh1 });
            // Create a separator before the Refresh item.
            itemRefresh1.Links[0].BeginGroup = true;
            // Process item clicks.
            // Associate the popup menu with the form.
            barManager1.SetPopupContextMenu(this, menu);
            menu.ShowPopup(Control.MousePosition);
        }
        private void ItemRefresh1_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogResult dr;
            dr = XtraMessageBox.Show("Bạn muốn ngừng kinh doanh sản phẩm này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                try
                {
                    var model = hangHoaService.GetById(int.Parse(grViewChild.GetRowCellValue(index1, grViewChild.Columns["ID"]).ToString()));
                    model.Status = false;
                    hangHoaService.Update(model);

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private void btnQuanLy_Click(object sender, EventArgs e)
        {
            barManager1.ForceInitialize();
            DevExpress.XtraBars.PopupMenu menu = new DevExpress.XtraBars.PopupMenu();
            menu.Manager = barManager1;
            //barManager1.Images = imageCollection1;
            BarButtonItem itemExport = new BarButtonItem(barManager1, "Xuất Excel",1);
            itemExport.ImageOptions.Image = DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/movedown_16x16.png");
            itemExport.ItemClick += ItemExport_ItemClick;
            BarButtonItem itemImport = new BarButtonItem(barManager1, "Nhập Excel",0);
            itemExport.ImageOptions.Image = DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/moveup_16x16.png");
            itemImport.ItemClick += ItemImport_ItemClick;
            BarButtonItem itemNhomHH = new BarButtonItem(barManager1, "Nhóm hàng hóa", 0);
            itemNhomHH.ItemClick += ItemNhomHH_ItemClick;
            BarButtonItem itemNhomTT = new BarButtonItem(barManager1, "Nhóm thuộc tính", 0);
            itemNhomTT.ItemClick += ItemNhomTT_ItemClick;
            menu.AddItems(new BarItem[] { itemExport, itemImport,itemNhomHH,itemNhomTT });
            barManager1.SetPopupContextMenu(this, menu);
            menu.ShowPopup(Control.MousePosition);
        }

        private void ItemNhomTT_ItemClick(object sender, ItemClickEventArgs e)
        {          
            frmThuocTinhHH nhom1 = new frmThuocTinhHH();
            nhom1.ShowDialog();
        }

        private void ItemNhomHH_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmDanhMucHH nhom = new frmDanhMucHH();
            nhom.ShowDialog();
        }
        private void ItemImport_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenFileDialog fopen = new OpenFileDialog();
            fopen.Filter = "(Tất cả các tệp)|*.*|(Các tệp excel)|*xlsx";
            fopen.ShowDialog();
            if (fopen.FileName != "")
            {
                Excel.Application app = new Excel.Application();
                Excel.Workbook wb = app.Workbooks.Open(fopen.FileName);
                try
                {
                    Excel.Worksheet sheet = wb.Sheets[1];
                    Excel.Range range = sheet.UsedRange;
                    int rows = range.Rows.Count;
                    int cols = range.Columns.Count;
                    maGiuld = Guid.NewGuid();
                    for (int i = 2; i <= rows; i++)
                    {                      
                            HangHoa hh = new HangHoa();
                            if (range.Cells[i, 7].Value != null)
                            {
                                if (i > 2 && range.Cells[i, 1].Value == range.Cells[i - 1, 1].Value && range.Cells[i, 1].Value != null)
                                {
                                    var cungloai = hangHoaService.GetByReadRootId(maGiuld.ToString());
                                    HangHoa item = new HangHoa();
                                    item.CheckHome = true;
                                    item.DonViTinh = cungloai.DonViTinh;
                                    item.GhiChu = cungloai.GhiChu;
                                    item.NhaSanXuat = cungloai.NhaSanXuat;
                                    item.NhomHangId = cungloai.NhomHangId;
                                    item.RootId = cungloai.RootId;
                                    item.Status = true;
                                    item.ThumbnalImage = cungloai.ThumbnalImage;
                                    if (range.Cells[i, 6].Value.ToString() == "0")
                                    {
                                        item.CheckVATBan = false;
                                        item.CheckVATMua = false;
                                        item.VAT = 0;
                                    }
                                    else
                                    {
                                        item.CheckVATBan = true;
                                        item.CheckVATMua = true;
                                        item.VAT = 1;
                                    }
                                    if (range.Cells[i, 4].Value != null)
                                        item.GiaBan = double.Parse(range.Cells[i, 4].Value.ToString());
                                    else
                                        item.GiaBan = 0;
                                    if (range.Cells[i, 3].Value != null)
                                        item.GiaMua = double.Parse(range.Cells[i, 3].Value.ToString());
                                    else
                                        item.GiaMua = 0;
                                    if (range.Cells[i, 5].Value != null)
                                        item.SoLuong = double.Parse(range.Cells[i, 5].Value.ToString());
                                    else
                                        item.SoLuong = 0;
                                    String MaHH = "";
                                    using (var ctx = new KTVDbContext())
                                    {
                                        var ListMa = ctx.Database.SqlQuery<GetMa>("exec CreateMa 'HangHoas'").FirstOrDefault();
                                        if (ListMa == null)
                                        {
                                            MessageBox.Show("Sinh mã lỗi, thử lại sau.");
                                        }
                                        MaHH = ListMa.Ma;
                                    }
                                    item.MaHang = MaHH;
                                    item.TenHang = range.Cells[i, 1].Value.ToString();
                                    string[] objlist = range.Cells[i, 7].Value.Split('|');
                                    for (int m = 0; m < objlist.Count(); m++)
                                    {
                                        int index = objlist[m].LastIndexOf(':') + 1;
                                        int index1 = objlist[m].Count();
                                        string chuoi = objlist[m].Substring(0, index - 1);
                                        string chuoicon = objlist[m].Substring(index, index1 - index);
                                        item.TenHang = item.TenHang + " " + chuoicon;
                                        DanhSach newitem = new DanhSach();
                                        newitem.Name = chuoi;
                                        newitem.Values = chuoicon;
                                        switch (m + 1)
                                        {
                                            case 1:
                                                if (!list1.Exists(x => x.Values == chuoicon))
                                                {
                                                    list1.Add(newitem);
                                                    gt1 += ";" + chuoicon;
                                                }
                                                break;
                                            case 2:
                                                if (!list2.Exists(x => x.Values == chuoicon))
                                                {
                                                    list2.Add(newitem);
                                                    gt2 += ";" + chuoicon;
                                                }
                                                break;
                                            case 3:
                                                if (!list3.Exists(x => x.Values == chuoicon))
                                                {
                                                    list3.Add(newitem);
                                                    gt3 += ";" + chuoicon;
                                                }
                                                break;
                                            case 4:
                                                if (!list4.Exists(x => x.Values == chuoicon))
                                                {
                                                    list4.Add(newitem);
                                                    gt4 += ";" + chuoicon;
                                                }
                                                break;
                                        }
                                    }
                                    hangHoaService.Add(item);
                                    var hh1 = hangHoaService.GetByRootId(maGiuld.ToString());
                                    hh1.SoLuong += item.SoLuong;
                                    hangHoaService.Update(hh1);
                                }
                                else
                                {
                                    List<ListThuocTinh> listTT = new List<ListThuocTinh>();
                                    if (list1.Count > 0)
                                    {
                                        var tt = thuocTinhService.GetByName(list1.FirstOrDefault().Name);
                                        listTT.Add(new ListThuocTinh
                                        {
                                            ID = tt.Id,
                                            Values = gt1
                                        });
                                    }
                                    if (list2.Count > 0)
                                    {
                                        var tt = thuocTinhService.GetByName(list2.FirstOrDefault().Name);
                                        listTT.Add(new ListThuocTinh
                                        {
                                            ID = tt.Id,
                                            Values = gt2
                                        });
                                    }
                                    if (list3.Count > 0)
                                    {
                                        var tt = thuocTinhService.GetByName(list3.FirstOrDefault().Name);
                                        listTT.Add(new ListThuocTinh
                                        {
                                            ID = tt.Id,
                                            Values = gt3
                                        });
                                    }
                                    if (list4.Count > 0)
                                    {
                                        var tt = thuocTinhService.GetByName(list4.FirstOrDefault().Name);
                                        listTT.Add(new ListThuocTinh
                                        {
                                            ID = tt.Id,
                                            Values = gt4
                                        });
                                    }
                                    if (listTT.Count > 0)
                                    {
                                        ttGroupService.AddThuocTinhByProduct(listTT, maGiuld.ToString());
                                        list1.Clear();
                                        list2.Clear();
                                        list3.Clear();
                                        list4.Clear();
                                        listTT.Clear();
                                    }
                                    HangHoa item1 = new HangHoa();
                                    HangHoa item = new HangHoa();
                                    maGiuld = Guid.NewGuid();
                                    if (range.Cells[i, 6].Value.ToString() == "0")
                                    {
                                        item1.CheckVATBan = false;
                                        item1.CheckVATMua = false;
                                        item1.VAT = 0;
                                        item.CheckVATBan = false;
                                        item.CheckVATMua = false;
                                        item.VAT = 0;
                                    }
                                    else
                                    {
                                        item1.CheckVATBan = true;
                                        item1.CheckVATMua = true;
                                        item1.VAT = 1;
                                        item.CheckVATBan = true;
                                        item.CheckVATMua = true;
                                        item.VAT = 1;
                                    }
                                    item1.XuatXu = "";
                                    item1.GhiChu = "";
                                    item.XuatXu = "";
                                    item.GhiChu = "";
                                    if (range.Cells[i, 12].Value != null)
                                    {
                                        item1.ThumbnalImage = range.Cells[i, 12].Value.ToString();
                                        item.ThumbnalImage = range.Cells[i, 12].Value.ToString();
                                    }
                                    if (range.Cells[i, 4].Value != null)
                                    {
                                        item1.GiaBan = double.Parse(range.Cells[i, 4].Value.ToString());
                                        item.GiaBan = double.Parse(range.Cells[i, 4].Value.ToString());
                                    }
                                    else
                                    {
                                        item1.GiaBan = 0;
                                        item.GiaBan = 0;
                                    }
                                    if (range.Cells[i, 3].Value != null)
                                    {
                                        item1.GiaMua = double.Parse(range.Cells[i, 3].Value.ToString());
                                        item.GiaMua = double.Parse(range.Cells[i, 3].Value.ToString());
                                    }
                                    else
                                    {
                                        item1.GiaMua = 0;
                                        item.GiaMua = 0;
                                    }
                                    item1.RootId = maGiuld.ToString();
                                    item1.TenHang = range.Cells[i, 1].Value.ToString();
                                    item1.CheckHome = true;
                                    item1.NhaSanXuat = range.Cells[i, 10].Value.ToString();
                                    item.RootId = maGiuld.ToString();
                                    item.TenHang = range.Cells[i, 1].Value.ToString();
                                    item.CheckHome = true;
                                    item.NhaSanXuat = range.Cells[i, 10].Value.ToString();
                                    if (range.Cells[i, 9].Value != null)
                                    {
                                        string[] obj = range.Cells[i, 9].Value.Split('>');
                                        if (nhomHHService.CheckExit(obj[0]) == true && obj.Count() > 1)
                                        {
                                            NhomHangHoa nhom = new NhomHangHoa();
                                            nhom.Name = obj[0];
                                            nhom.NhomHangId = 0;
                                            nhom.Stt = 0;
                                            var nhh = nhomHHService.Add(nhom);
                                            for (int j = 1; j < obj.Count(); j++)
                                            {
                                                if (nhomHHService.CheckExit(obj[j]) == true)
                                                {
                                                    NhomHangHoa nhom1 = new NhomHangHoa();
                                                    nhom1.Name = obj[j];
                                                    nhom1.NhomHangId = nhh.Id;
                                                    nhh = nhomHHService.Add(nhom1);
                                                    item1.NhomHangId = nhh.Id;
                                                }
                                                else
                                                {
                                                    nhh = nhomHHService.GetByName(obj[j]);
                                                    item1.NhomHangId = nhh.Id;
                                                    item.NhomHangId = nhh.Id;
                                                }
                                            }
                                        }
                                        else if (obj.Count() > 1)
                                        {
                                            var nhh = nhomHHService.GetByName(obj[0]);
                                            for (int j = 1; j < obj.Count(); j++)
                                            {
                                                if (nhomHHService.CheckExit(obj[j]) == true)
                                                {
                                                    NhomHangHoa nhom1 = new NhomHangHoa();
                                                    nhom1.Name = obj[j];
                                                    nhom1.NhomHangId = nhh.Id;
                                                    nhh = nhomHHService.Add(nhom1);
                                                    item1.NhomHangId = nhh.Id;
                                                    item.NhomHangId = nhh.Id;
                                                }
                                                else
                                                {
                                                    nhh = nhomHHService.GetByName(obj[0]);
                                                    item1.NhomHangId = nhh.Id;
                                                    item.NhomHangId = nhh.Id;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var nhh = nhomHHService.GetByName(obj[0]);
                                            if (nhh != null)
                                            {
                                                item1.NhomHangId = nhh.Id;
                                                item.NhomHangId = nhh.Id;
                                            }
                                            else
                                            {
                                                NhomHangHoa nhom = new NhomHangHoa();
                                                nhom.Name = obj[0];
                                                nhom.NhomHangId = 0;
                                                nhom.Stt = 0;
                                                var nhh1 = nhomHHService.Add(nhom);
                                                item1.NhomHangId = nhh1.Id;
                                                item.NhomHangId = nhh1.Id;
                                            }
                                        }
                                    }
                                    item1.Status = true;
                                    item.Status = true;
                                    if (range.Cells[i, 12].Value != null)
                                    {
                                        item1.ThumbnalImage = range.Cells[i, 12].Value.ToString();
                                        item.ThumbnalImage = range.Cells[i, 12].Value.ToString();
                                    }
                                    if (range.Cells[i, 5].Value != null)
                                    {
                                        item1.SoLuong = double.Parse(range.Cells[i, 5].Value.ToString());
                                        item.SoLuong = double.Parse(range.Cells[i, 5].Value.ToString());
                                    }
                                    else
                                    {
                                        item1.SoLuong = 0;
                                        item.SoLuong = 0;
                                    }
                                    if (hangHoaService.CheckExit(item1.TenHang) == true)
                                    {
                                    hangHoaService.Add(item1);
                                    String MaHH = "";
                                    using (var ctx = new KTVDbContext())
                                    {
                                        var ListMa = ctx.Database.SqlQuery<GetMa>("exec CreateMa 'HangHoas'").FirstOrDefault();
                                        if (ListMa == null)
                                        {
                                            MessageBox.Show("Sinh mã lỗi, thử lại sau.");
                                        }
                                        MaHH = ListMa.Ma;
                                    }
                                    item.MaHang = MaHH;
                                    string[] objlist = range.Cells[i, 7].Value.Split('|');
                                    for (int m = 0; m < objlist.Count(); m++)
                                    {
                                        int index = objlist[m].LastIndexOf(':') + 1;
                                        int index1 = objlist[m].Count();
                                        string chuoi = objlist[m].Substring(0, index - 1);
                                        string chuoicon = objlist[m].Substring(index, index1 - index);
                                        DanhSach newItem = new DanhSach();
                                        newItem.Name = chuoi;
                                        newItem.Values = chuoicon;
                                        switch (m)
                                        {
                                            case 0:
                                                list1.Add(newItem);
                                                gt1 = chuoicon;
                                                break;
                                            case 1:
                                                list2.Add(newItem);
                                                gt2 = chuoicon;
                                                break;
                                            case 2:
                                                list3.Add(newItem);
                                                gt3 = chuoicon;
                                                break;
                                            case 3:
                                                list4.Add(newItem);
                                                gt4 = chuoicon;
                                                break;
                                        }
                                        item.TenHang = item.TenHang + " " + chuoicon;
                                    }
                                    hangHoaService.Add(item);
                                }                                 
                                }
                            }
                            else
                            {
                                List<ListThuocTinh> listTT = new List<ListThuocTinh>();
                                if (list1.Count > 0)
                                {
                                    var tt = thuocTinhService.GetByName(list1.FirstOrDefault().Name);
                                    listTT.Add(new ListThuocTinh
                                    {
                                        ID = tt.Id,
                                        Values = gt1
                                    });
                                }
                                if (list2.Count > 0)
                                {

                                    var tt = thuocTinhService.GetByName(list2.FirstOrDefault().Name);
                                    listTT.Add(new ListThuocTinh
                                    {
                                        ID = tt.Id,
                                        Values = gt2
                                    });
                                }
                                if (list3.Count > 0)
                                {
                                    var tt = thuocTinhService.GetByName(list3.FirstOrDefault().Name);
                                    listTT.Add(new ListThuocTinh
                                    {
                                        ID = tt.Id,
                                        Values = gt3
                                    });
                                }
                                if (list4.Count > 0)
                                {
                                    var tt = thuocTinhService.GetByName(list4.FirstOrDefault().Name);
                                    listTT.Add(new ListThuocTinh
                                    {
                                        ID = tt.Id,
                                        Values = gt4
                                    });
                                }
                                if (listTT.Count > 0)
                                {
                                    ttGroupService.AddThuocTinhByProduct(listTT, maGiuld.ToString());
                                    list1.Clear();
                                    list2.Clear();
                                    list3.Clear();
                                    list4.Clear();
                                    listTT.Clear();
                                }
                                HangHoa item = new HangHoa();
                                if (range.Cells[i, 6].Value.ToString() == "0")
                                {
                                    item.CheckVATBan = false;
                                    item.CheckVATMua = false;
                                    item.VAT = 0;
                                }
                                else
                                {
                                    item.CheckVATBan = true;
                                    item.CheckVATMua = true;
                                    item.VAT = 1;
                                }
                                item.XuatXu = "";
                                item.GhiChu = "";
                                if (range.Cells[i, 12].Value != null)
                                    item.ThumbnalImage = range.Cells[i, 12].Value.ToString();
                                if (range.Cells[i, 4].Value != null)
                                    item.GiaBan = double.Parse(range.Cells[i, 4].Value.ToString());
                                else
                                    item.GiaBan = 0;
                                if (range.Cells[i, 3].Value != null)
                                    item.GiaMua = double.Parse(range.Cells[i, 3].Value.ToString());
                                else
                                    item.GiaMua = 0;
                                item.TenHang = range.Cells[i, 1].Value.ToString();
                                item.CheckHome = true;
                                String MaHH = "";
                                using (var ctx = new KTVDbContext())
                                {
                                    var ListMa = ctx.Database.SqlQuery<GetMa>("exec CreateMa 'HangHoas'").FirstOrDefault();
                                    if (ListMa == null)
                                    {
                                        MessageBox.Show("Sinh mã lỗi, thử lại sau.");
                                    }
                                    MaHH = ListMa.Ma;
                                }
                                item.MaHang = MaHH;
                                item.NhaSanXuat = range.Cells[i, 10].Value.ToString();
                                if (range.Cells[i, 9].Value != null)
                                {
                                    string[] obj = range.Cells[i, 9].Value.ToString().Split('>');
                                    if (obj.Count() > 1)
                                    {
                                        NhomHangHoa nhom = new NhomHangHoa();
                                        nhom.Name = obj[0];
                                        nhom.NhomHangId = 0;
                                        nhom.Stt = 0;
                                        var nhh = nhomHHService.Add(nhom);
                                        for (int j = 1; j < obj.Count(); j++)
                                        {
                                            if (nhomHHService.CheckExit(obj[j]) == true)
                                            {
                                                NhomHangHoa nhom1 = new NhomHangHoa();
                                                nhom1.Name = obj[j];
                                                nhom1.NhomHangId = nhh.Id;
                                                nhh = nhomHHService.Add(nhom1);
                                                if (j == obj.Count() - 1)
                                                {
                                                    item.NhomHangId = nhh.Id;
                                                }
                                            }
                                            else
                                            {
                                                nhh = nhomHHService.GetByName(obj[j]);
                                                item.NhomHangId = nhh.Id;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (nhomHHService.CheckExit(obj[0]) == true)
                                        {
                                            NhomHangHoa nhom = new NhomHangHoa();
                                            nhom.Name = obj[0];
                                            nhom.NhomHangId = 0;
                                            nhom.Stt = 0;
                                            var nhh = nhomHHService.Add(nhom);
                                            item.NhomHangId = nhh.Id;
                                        }
                                        else
                                        {
                                            var nhh = nhomHHService.GetByName(obj[0]);
                                            item.NhomHangId = nhh.Id;
                                        }
                                    }
                                }
                                item.Status = true;
                                if (range.Cells[i, 12].Value != null)
                                    item.ThumbnalImage = range.Cells[i, 12].Value.ToString();
                                if (range.Cells[i, 5].Value != null)
                                    item.SoLuong = double.Parse(range.Cells[i, 5].Value.ToString());
                                else
                                    item.SoLuong = 0;
                                if (hangHoaService.CheckExit(item.TenHang) == true)
                                {
                                    hangHoaService.Add(item);
                                }                                
                            }                                           
                    }
                    XtraMessageBox.Show("Nhập thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    GetData();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
                XtraMessageBox.Show("Bạn không chọn tệp tin nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Question);
        }

        private void ItemExport_ItemClick(object sender, ItemClickEventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            f.Filter = "Execl files (*.xls)|*.xls";
            f.FileName = "DanhSachHangHoa.xlsx";
            f.FilterIndex = 0;
            f.RestoreDirectory = true;
            f.CreatePrompt = true;
            f.Title = "Export Excel File To";
            if (f.ShowDialog() == DialogResult.OK)
            {
                string path = System.IO.Path.GetFullPath("DSHH.xlsx");             
                using (FileStream templateDocumentStream = File.OpenRead(path))
                {
                    using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                    {
                        ExcelWorksheet sheet = package.Workbook.Worksheets[1];
                        sheet.Cells[4, 4].Value = DateTime.Now.ToString("dd/MM/yyyy");
                        var listParents = hangHoaService.GetAllParent();
                        int index = 7;
                        for (int i = 0; i < listParents.Count(); i++)
                        {
                            string ten = "";
                            if (i == 0)
                            {
                                if (listParents.ElementAt(i).NhomHangId > 0)
                                {
                                    ten = nhomHHService.GetById(listParents.ElementAt(i).NhomHangId).Name;
                                }
                                if (listParents.ElementAt(i).RootId != null)
                                {
                                    var listChild = hangHoaService.GetListByRootId(listParents.ElementAt(i).RootId);
                                    sheet.Cells[index, 1].Value = i + 1;
                                    sheet.Cells[index, 2].Value = listParents.ElementAt(i).TenHang;
                                    sheet.Cells[index, 3].Value = listParents.ElementAt(i).SoLuong;
                                    sheet.Cells[index, 4].Value = 0;
                                    sheet.Cells[index, 5].Value = ten;
                                    sheet.Cells[index, 6].Value = listParents.ElementAt(i).NhaSanXuat;
                                    sheet.Cells[index, 1, index + listChild.Count-1, 1].Merge = true;
                                    sheet.Cells[index, 1, index + listChild.Count-1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[index, 1, index + listChild.Count-1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 2, index + listChild.Count-1, 2].Merge = true;
                                    sheet.Cells[index, 2, index + listChild.Count-1, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    sheet.Cells[index, 2, index + listChild.Count-1, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 3, index + listChild.Count-1, 3].Merge = true;
                                    sheet.Cells[index, 3, index + listChild.Count-1, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[index, 3, index + listChild.Count-1, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 4, index + listChild.Count-1, 4].Merge = true;
                                    sheet.Cells[index, 4, index + listChild.Count-1, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[index, 4, index + listChild.Count-1, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 5, index + listChild.Count-1, 5].Merge = true;
                                    sheet.Cells[index, 5, index + listChild.Count-1, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[index, 5, index + listChild.Count-1, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 6, index + listChild.Count-1, 6].Merge = true;
                                    sheet.Cells[index, 6, index + listChild.Count-1, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[index, 6, index + listChild.Count-1, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    for (int j = 0; j < listChild.Count; j++)
                                    {
                                        sheet.Cells[index + j, 7].Value = listChild.ElementAt(j).TenHang;
                                        sheet.Cells[index + j, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        sheet.Cells[index + j, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        sheet.Cells[index + j, 8].Value = listChild.ElementAt(j).MaHang;
                                        sheet.Cells[index + j, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        sheet.Cells[index + j, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        sheet.Cells[index + j, 9].Value = listChild.ElementAt(j).SoLuong;
                                        sheet.Cells[index + j, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        sheet.Cells[index + j, 9].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        sheet.Cells[index + j, 10].Value = listChild.ElementAt(j).GiaMua;
                                        sheet.Cells[index + j, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        sheet.Cells[index + j, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        sheet.Cells[index + j, 11].Value = listChild.ElementAt(j).GiaBan;
                                        sheet.Cells[index + j, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        sheet.Cells[index + j, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }
                                    index +=listChild.Count;
                                }
                                else
                                {
                                    sheet.Cells[i + 7, 1].Value = i + 1;
                                    sheet.Cells[i + 7, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[i + 7, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[i + 7, 2].Value = listParents.ElementAt(i).TenHang;
                                    sheet.Cells[i + 7, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    sheet.Cells[i + 7, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[i + 7, 3].Value = listParents.ElementAt(i).SoLuong;
                                    sheet.Cells[i + 7, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[i + 7, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[i + 7, 4].Value = 0;
                                    sheet.Cells[i + 7, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[i + 7, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[i + 7, 5].Value = ten;
                                    sheet.Cells[i + 7, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[i + 7, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[i + 7, 6].Value = listParents.ElementAt(i).NhaSanXuat;
                                    sheet.Cells[i + 7, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[i + 7, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[i + 7, 7].Value = listParents.ElementAt(i).TenHang;
                                    sheet.Cells[i + 7, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    sheet.Cells[i + 7, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[i + 7, 8].Value = listParents.ElementAt(i).MaHang;
                                    sheet.Cells[i + 7, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[i + 7, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[i + 7, 9].Value = listParents.ElementAt(i).SoLuong;
                                    sheet.Cells[i + 7, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[i + 7, 9].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[i + 7, 10].Value = listParents.ElementAt(i).GiaMua;
                                    sheet.Cells[i + 7, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[i + 7, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[i + 7, 11].Value = listParents.ElementAt(i).GiaBan;
                                    sheet.Cells[i + 7, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[i + 7,11].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    index += 1;
                                }
                            }
                            else
                            {
                                if (listParents.ElementAt(i).NhomHangId > 0)
                                {
                                    ten = nhomHHService.GetById(listParents.ElementAt(i).NhomHangId).Name;
                                }
                                if (listParents.ElementAt(i).RootId != null)
                                {
                                    //ws.Cells[Rowstart, ColStart, RowEnd, ColEnd]
                                    var listChild = hangHoaService.GetListByRootId(listParents.ElementAt(i).RootId);
                                    sheet.Cells[index, 1].Value = i + 1;
                                    sheet.Cells[index, 2].Value = listParents.ElementAt(i).TenHang;
                                    sheet.Cells[index, 3].Value = listParents.ElementAt(i).SoLuong;
                                    sheet.Cells[index, 4].Value = 0;
                                    sheet.Cells[index, 5].Value = ten;
                                    sheet.Cells[index, 6].Value = listParents.ElementAt(i).NhaSanXuat;
                                    sheet.Cells[index,1,index + listChild.Count-1, 1].Merge = true;
                                    sheet.Cells[index, 1, index + listChild.Count-1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[index, 1, index + listChild.Count-1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 2, index + listChild.Count-1, 2].Merge = true;
                                    sheet.Cells[index, 2, index + listChild.Count-1, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    sheet.Cells[index, 2, index + listChild.Count-1, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 3, index + listChild.Count-1, 3].Merge = true;
                                    sheet.Cells[index, 3, index + listChild.Count-1, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[index, 3, index + listChild.Count-1, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 4, index + listChild.Count-1, 4].Merge = true;
                                    sheet.Cells[index, 4, index + listChild.Count-1, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[index, 4, index + listChild.Count-1, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 5, index + listChild.Count-1, 5].Merge = true;
                                    sheet.Cells[index, 5, index + listChild.Count-1, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[index, 5, index + listChild.Count-1, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 6, index + listChild.Count-1, 6].Merge = true;
                                    sheet.Cells[index, 6, index + listChild.Count-1, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[index, 6, index + listChild.Count-1, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    for (int j = 0; j < listChild.Count; j++)
                                    {
                                        sheet.Cells[index + j, 7].Value = listChild.ElementAt(j).TenHang;
                                        sheet.Cells[index + j, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        sheet.Cells[index + j, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        sheet.Cells[index + j, 8].Value = listChild.ElementAt(j).MaHang;
                                        sheet.Cells[index + j, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        sheet.Cells[index + j, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        sheet.Cells[index + j, 9].Value = listChild.ElementAt(j).SoLuong;
                                        sheet.Cells[index + j, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        sheet.Cells[index + j, 9].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        sheet.Cells[index + j, 10].Value = listChild.ElementAt(j).GiaMua;
                                        sheet.Cells[index + j, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        sheet.Cells[index + j, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        sheet.Cells[index + j, 11].Value = listChild.ElementAt(j).GiaBan;
                                        sheet.Cells[index + j, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        sheet.Cells[index + j, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    }
                                    index +=listChild.Count;
                                }
                                else
                                {
                                    sheet.Cells[index, 1].Value = i + 1;
                                    sheet.Cells[index, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[index, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 2].Value = listParents.ElementAt(i).TenHang;
                                    sheet.Cells[index, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    sheet.Cells[index, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 3].Value = listParents.ElementAt(i).SoLuong;
                                    sheet.Cells[index, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[index, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 4].Value = 0;
                                    sheet.Cells[index, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[index, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 5].Value = ten;
                                    sheet.Cells[index, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[index, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 6].Value = listParents.ElementAt(i).NhaSanXuat;
                                    sheet.Cells[index, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[index, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 7].Value = listParents.ElementAt(i).TenHang;
                                    sheet.Cells[index, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    sheet.Cells[index, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 8].Value = listParents.ElementAt(i).MaHang;
                                    sheet.Cells[index, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    sheet.Cells[index, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 9].Value = listParents.ElementAt(i).SoLuong;
                                    sheet.Cells[index, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[index, 9].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 10].Value = listParents.ElementAt(i).GiaMua;
                                    sheet.Cells[index, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[index, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    sheet.Cells[index, 11].Value = listParents.ElementAt(i).GiaBan;
                                    sheet.Cells[index, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[index, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    index += 1;
                                }
                            }
                        }
                        package.SaveAs(new FileInfo(f.FileName));
                    }                   
                }               
            }           
        }
    }
}