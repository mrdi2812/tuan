﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using KTV.Service;
using KTV.Model;
using System.IO;
using DevExpress.XtraEditors.Controls;
using KTV.Common;
using KTV.Data;
using System.Net;

namespace KTV.App
{
    public partial class frmEditHangHoa : DevExpress.XtraEditors.XtraForm
    {
        List<ListThuocTinh> listTT = new List<ListThuocTinh>();
        List<int> cbList = new List<int>();
        List<string> textList = new List<string>();
        int cb1 = 0, cb2 = 0, cb3 = 0, cb4 = 0;
        List<string> list1, list2, list3, list4;
        string giatri1, giatri2, giatri3, giatri4, gt1, gt2, gt3, gt4;
        List<HangHoa> data = new List<HangHoa>();
        List<HangHoa> data1 = new List<HangHoa>();
        List<HangHoaVM> newList = new List<HangHoaVM>();
        NhaSXService nhaSXService = new NhaSXService();
        NhomHHService nhomHHService = new NhomHHService();
        ThuocTinhService thuocTinhService = new ThuocTinhService();
        HangHoaService hangHoaService = new HangHoaService();
        DonViTinhService donViTinhService = new DonViTinhService();
        ThuocTinhGroupService ttGroupService = new ThuocTinhGroupService();
        Guid maGiuld;
        OpenFileDialog fileOpen = new OpenFileDialog();
        SaveFileDialog fileSave = new SaveFileDialog();
        string urlImage = "";
        string urlPath = "";
        int index;
        int txtTen = 0;
        string IPMAYCHU;
        int countControl = 0;
        public int ID { set; get; }
        public string Thaotac { set; get; }
        public frmEditHangHoa()
        {
            InitializeComponent();
            layoutControl3.Visible = false;
            layoutControl4.Visible = false;
            layoutControl2.Visible = false;
            IPHostEntry Ip = Dns.GetHostByName(Dns.GetHostName());
            foreach (IPAddress ip in Ip.AddressList)
            {
                IPMAYCHU = ip.ToString();
            }
        }

        private void cbThuocTinh_CheckedChanged(object sender, EventArgs e)
        {          
            if (cbThuocTinh.Checked == true && txtGiaBan.Text.All(Char.IsDigit) == true && txtGiaNhap.Text.All(Char.IsDigit) == true && txtTenHH.Text != null && txtTenHH.Text != "")
            {
 
                layoutControl3.Visible = true;
                layoutControl2.Visible = true;
                layoutControl4.Visible = true;
                btnEdit.Buttons[0].Enabled = false;
                txtMaHH.Enabled = false;
                int count = groupControlTT.Controls.OfType<GridLookUpEdit>().Count();
                int count1 = groupControlTT.Controls.OfType<TextEdit>().Count();
                if (count == 0)
                {
                    int xcb = 12;
                    int ycb = 12;
                    int xtag = 168;
                    int ytag = 12;
                    GridLookUpEdit comboBoxEdit = new GridLookUpEdit();
                    comboBoxEdit.Name = "cb" + count + 1;
                    comboBoxEdit.Properties.DataSource = LoadDL2();
                    comboBoxEdit.Properties.ValueMember = "ID";
                    comboBoxEdit.Properties.DisplayMember = "Ten";
                    comboBoxEdit.Text = LoadDL2().Rows[LoadDL2().Rows.Count - 1][0].ToString();
                    comboBoxEdit.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                    comboBoxEdit.Properties.ImmediatePopup = true;
                    comboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                    comboBoxEdit.Location = new System.Drawing.Point(xcb, ycb + 25 * (count + 1));
                    comboBoxEdit.MinimumSize = new Size(152, 25);
                    comboBoxEdit.Size = new System.Drawing.Size(152, 25);
                    if (Thaotac != "Edit"&&Thaotac!="Copy")
                    {
                        groupControlTT.Controls.Add(comboBoxEdit);
                    }                   
                    TextBox tagListControl = new TextBox();
                    tagListControl.Name = "taglist" + count1 + 1;
                    tagListControl.Location = new System.Drawing.Point(xtag, ytag + 25 * (count + 1));
                    tagListControl.MinimumSize = new Size(1000, 25);
                    tagListControl.Size = new System.Drawing.Size(1000, 25);
                    tagListControl.KeyPress += new KeyPressEventHandler(text_KeyPress);
                    if (Thaotac != "Edit" && Thaotac != "Copy")
                    {
                        groupControlTT.Controls.Add(tagListControl);
                    }
                    
                }

            }
            else
            {
                MessageBox.Show("Vui lòng khai báo tên hàng hóa");
                layoutControl3.Visible = false;
                layoutControl4.Visible = false;
                layoutControl2.Visible = false;
                txtMaHH.Enabled = false;
            }
        }

        private void cbVATGV_CheckedChanged(object sender, EventArgs e)
        {
            if (cbVATGV.Checked == true)
            {
                cbVATGB.Checked = true;
            }
            else
            {
                cbVATGB.Checked = false;
            }
        }

        private void txtGiaNhap_Validated(object sender, EventArgs e)
        {
            if (txtGiaNhap.Text.All(Char.IsNumber) == false)
            {
                MessageBox.Show("Vui lòng nhập kiểu số");
                txtGiaNhap.Text = "";
            }
            if (txtGiaNhap.Text == "Giá nhập" || string.IsNullOrEmpty(txtGiaNhap.Text))
            {
                MessageBox.Show("Không được để trống");
                txtGiaNhap.Text = "0";
            }
        }

        private void txtGiaBan_Validated(object sender, EventArgs e)
        {
            if (txtGiaBan.Text.All(Char.IsNumber) == false)
            {
                MessageBox.Show("Vui lòng nhập kiểu số");
                txtGiaBan.Text = "0";
            }
        }

        private void txtPhanTram_Validated(object sender, EventArgs e)
        {
            if (txtPhanTram.Text.All(Char.IsNumber) == false)
            {
                MessageBox.Show("Vui lòng nhập kiểu số");
                txtPhanTram.Text = "0";
            }
        }

        private void txtGiaNhap_TextChanged(object sender, EventArgs e)
        {
            if (txtPhanTram.Text.All(Char.IsDigit) == true && txtGiaNhap.Text.All(Char.IsDigit) == true && !string.IsNullOrEmpty(txtPhanTram.Text) && !string.IsNullOrEmpty(txtGiaNhap.Text))
            {
                txtGiaBan.Text = (float.Parse(txtGiaNhap.Text) + float.Parse(txtGiaNhap.Text) * (float.Parse(txtPhanTram.Text) / 100)).ToString();
                for (int i = 0; i < grView.RowCount; i++)
                {
                    int row = grView.GetVisibleRowHandle(i);                  
                    grView.SetRowCellValue(row, "GiaMua", txtGiaNhap.Text);
                    grView.SetRowCellValue(row, "GiaBan", txtGiaBan.Text);
                }
            }
            else if(Thaotac!="Copy"||Thaotac!="Edit")
            {
                txtGiaBan.Text = "0";
            }
        }

        private void txtPhanTram_TextChanged(object sender, EventArgs e)
        {
            if (txtPhanTram.Text.All(Char.IsDigit) == true && txtGiaNhap.Text.All(Char.IsDigit) == true && !string.IsNullOrEmpty(txtPhanTram.Text) && !string.IsNullOrEmpty(txtGiaNhap.Text))
            {
                txtGiaBan.Text = (float.Parse(txtGiaNhap.Text) + float.Parse(txtGiaNhap.Text) * (float.Parse(txtPhanTram.Text) / 100)).ToString();
                for (int i = 0; i < grView.RowCount; i++)
                {
                    int row = grView.GetVisibleRowHandle(i);
                    grView.SetRowCellValue(row, "GiaMua", txtGiaNhap.Text);
                    grView.SetRowCellValue(row, "GiaBan", txtGiaBan.Text);
                }
            }
            else if (Thaotac != "Copy" || Thaotac != "Edit")
            {
                txtGiaBan.Text = "0";
            }
        }

        private void txtTonKho_Validated(object sender, EventArgs e)
        {
            if (txtTonKho.Text.All(Char.IsDigit) == false)
            {
                MessageBox.Show("Vui lòng nhập kiểu số");
                txtTonKho.Text = "";
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            int countad = grView.RowCount;
            int m = data.Count;
            btnAddTT.Enabled = false;
            simpleButton5.Enabled = false;
            if (btnLuuTT.Enabled == true && Thaotac != "Copy")
            {
                txtTen = txtTenHH.Text.Count();
                cbThuocTinh.Enabled = false;
                for (int i=m; i < countad; i++)
                {
                    HangHoa item = new HangHoa();
                    //int max = hangHoaService.CheckMax();
                    int index = grView.GetVisibleRowHandle(i);
                    String MaHH = "";
                    using (var ctx = new KTVDbContext())
                    {
                        var ListMa = ctx.Database.SqlQuery<GetMa>("exec CreateMa 'HangHoas','MaHang'").FirstOrDefault();
                        if (ListMa == null)
                        {
                            MessageBox.Show("Sinh mã lỗi, thử lại sau.");
                        }
                        MaHH = ListMa.Ma;
                    }
                    item.MaHang = MaHH;
                    //item.MaHang = SinhMaTuDong(max + 1);
                    item.CheckVATBan = cbVATGB.Checked;
                    item.CheckVATMua = cbVATGV.Checked;               
                    item.GiaBan = double.Parse(grView.GetRowCellValue(index, "GiaBan").ToString());
                    item.GiaMua = double.Parse(grView.GetRowCellValue(index,"GiaMua").ToString());
                    item.RootId = maGiuld.ToString();
                    item.TenHang = grView.GetRowCellValue(index, "TenHang").ToString();
                    item.CheckHome = cbHienThi.Checked;
                    item.NhaSanXuat = cbNhaSX.Text;
                    if (cbDanhMuc.EditValue != null)
                    {
                        item.NhomHangId = int.Parse(cbDanhMuc.EditValue.ToString());
                    }
                    item.DonViTinh = cbDonViTinh.Text;
                    item.Status = true;
                    item.SoLuong = double.Parse(grView.GetRowCellValue(index, "SoLuong").ToString());                  
                    if (!data.Exists(x => x.TenHang.Trim() == item.TenHang.Trim()))
                    {
                        hangHoaService.Add(item);
                        data.Add(item);
                    }                       
                    if(!newList.Exists(x => x.TenHang.Trim() == item.TenHang.Trim()))
                    {
                        newList.Add(new HangHoaVM
                        {
                            MaHang = item.MaHang,
                            TenHang = item.TenHang,
                            GiaMua = item.GiaMua,
                            GiaBan = item.GiaBan,
                            SoLuong = item.SoLuong
                        });
                    }
                    else
                    {                       
                      int item1 = newList.FindIndex(x=>x.TenHang==item.TenHang);
                      newList[item1].MaHang = item.MaHang;
                    }
                }               
                if (grView.RowCount > 0)
                {
                    listTT.Clear();
                    int count = 1;
                    foreach (Control ctrl in groupControlTT.Controls)
                    {
                        if (ctrl is GridLookUpEdit)
                        {
                            switch (count)
                            {
                                case 1:
                                    cb1 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb1);
                                    textList.Add(gt1);
                                    listTT.Add(new ListThuocTinh { ID = cb1, Values = gt1 });
                                    break;
                                case 2:
                                    cb2 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb2);
                                    textList.Add(gt2);
                                    listTT.Add(new ListThuocTinh { ID = cb2, Values = gt2 });
                                    break;
                                case 3:
                                    cb3 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb3);
                                    textList.Add(gt3);
                                    listTT.Add(new ListThuocTinh { ID = cb3, Values = gt3 });
                                    break;
                                case 4:
                                    cb4 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb4);
                                    textList.Add(gt4);
                                    listTT.Add(new ListThuocTinh { ID = cb4, Values = gt4 });
                                    break;
                            }
                            count++;
                        }
                    }
                    ttGroupService.AddThuocTinhByProduct(listTT, maGiuld.ToString());
                }
                MessageBox.Show("Thành công");
                btnDelete.Buttons[0].Enabled = true;
                btnEdit.Buttons[0].Enabled = true;
                grControl.DataBindings.Clear();
                grControl.DataSource = data;
            }
            if (Thaotac == "Copy")
            {
                txtTen = txtTenHH.Text.Count();
                cbThuocTinh.Enabled = false;
                btnLuuTT.Enabled = true;
                HangHoa item1 = new HangHoa();
                item1.CheckVATBan = cbVATGB.Checked;
                item1.CheckVATMua = cbVATGV.Checked;
                item1.XuatXu = txtXuatXu.Text;
                item1.GhiChu = txtGhiChu.Text;
                item1.DonViTinh = cbDonViTinh.Text;
                if (!string.IsNullOrEmpty(urlImage))
                {
                    item1.ThumbnalImage = urlImage;
                }
                if (txtGiaBan.Text != null)
                    item1.GiaBan = double.Parse(txtGiaBan.Text);
                else
                    item1.GiaBan = 0;
                if (txtGiaNhap.Text != null)
                    item1.GiaMua = double.Parse(txtGiaNhap.Text);
                else
                    item1.GiaMua = 0;
                if (cbThuocTinh.Checked == true)
                {
                    item1.RootId = maGiuld.ToString();
                    item1.MaHang = null;
                }
                else
                {
                    String MaHH = "";
                    using (var ctx = new KTVDbContext())
                    {
                        var ListMa = ctx.Database.SqlQuery<GetMa>("exec CreateMa 'HangHoas','MaHang'").FirstOrDefault();
                        if (ListMa == null)
                        {
                            MessageBox.Show("Sinh mã lỗi, thử lại sau.");
                        }
                        MaHH = ListMa.Ma;
                    }
                    item1.MaHang = MaHH;
                    //item1.MaHang = SinhMaTuDong(hangHoaService.CheckMax() + 1);
                }
                item1.TenHang = txtTenHH.Text;
                item1.CheckHome = cbHienThi.Checked;
                item1.NhaSanXuat = cbNhaSX.Text;
                if (cbDanhMuc.EditValue != null)
                {
                    item1.NhomHangId = int.Parse(cbDanhMuc.EditValue.ToString());
                }
                item1.Status = true;
                if (!string.IsNullOrEmpty(urlImage))
                {
                    item1.ThumbnalImage = urlImage;
                }
                if (txtTonKho.Text != null)
                    item1.SoLuong = double.Parse(txtTonKho.Text);
                else
                    item1.SoLuong = 0;
                if (hangHoaService.CheckExit(item1.TenHang) == true)
                {
                    var model1 = hangHoaService.Add(item1);
                    hangHoaService.Save();
                    ID = model1.ID;
                }               
                for (int i = 0; i < grView.RowCount; i++)
                {
                    HangHoa item = new HangHoa();
                    //int max = hangHoaService.CheckMax();
                    int index = grView.GetVisibleRowHandle(i);
                    String MaHH = "";
                    using (var ctx = new KTVDbContext())
                    {
                        var ListMa = ctx.Database.SqlQuery<GetMa>("exec CreateMa 'HangHoas','MaHang'").FirstOrDefault();
                        if (ListMa == null)
                        {
                            MessageBox.Show("Sinh mã lỗi, thử lại sau.");
                        }
                        MaHH = ListMa.Ma;
                    }
                    item.DonViTinh = cbDonViTinh.Text;
                    item.MaHang = MaHH;
                    //item.MaHang = SinhMaTuDong(max + 1);
                    item.CheckVATBan = cbVATGB.Checked;
                    item.CheckVATMua = cbVATGV.Checked;
                    item.GiaBan = double.Parse(grView.GetRowCellValue(index, "GiaBan").ToString());
                    item.GiaMua = double.Parse(grView.GetRowCellValue(index, "GiaMua").ToString());
                    item.RootId = maGiuld.ToString();
                    item.TenHang = grView.GetRowCellValue(index, "TenHang").ToString();
                    item.CheckHome = cbHienThi.Checked;
                    item.NhaSanXuat = cbNhaSX.Text;
                    if (cbDanhMuc.EditValue != null)
                    {
                        item.NhomHangId = int.Parse(cbDanhMuc.EditValue.ToString());
                    }
                    item.Status = true;
                    item.SoLuong = double.Parse(grView.GetRowCellValue(index, "SoLuong").ToString());
                    if (!data.Exists(x => x.TenHang == item.TenHang))
                    {
                        hangHoaService.Add(item);
                        data.Add(item);
                    }
                    if (!newList.Exists(x => x.TenHang == item.TenHang))
                    {
                        newList.Add(new HangHoaVM
                        {
                            MaHang = item.MaHang,
                            TenHang = item.TenHang,
                            GiaMua = item.GiaMua,
                            GiaBan = item.GiaBan,
                            SoLuong = item.SoLuong
                        });
                    }
                    else
                    {
                        int item2 = newList.FindIndex(x => x.TenHang == item.TenHang);
                        newList[item2].MaHang = item.MaHang;
                    }
                }
                if (grView.RowCount > 0)
                {
                    listTT.Clear();
                    int count = 1;
                    foreach (Control ctrl in groupControlTT.Controls)
                    {
                        if (ctrl is GridLookUpEdit)
                        {
                            switch (count)
                            {
                                case 1:
                                    cb1 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb1);
                                    textList.Add(gt1);
                                    listTT.Add(new ListThuocTinh { ID = cb1, Values = gt1 });
                                    break;
                                case 2:
                                    cb2 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb2);
                                    textList.Add(gt2);
                                    listTT.Add(new ListThuocTinh { ID = cb2, Values = gt2 });
                                    break;
                                case 3:
                                    cb3 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb3);
                                    textList.Add(gt3);
                                    listTT.Add(new ListThuocTinh { ID = cb3, Values = gt3 });
                                    break;
                                case 4:
                                    cb4 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb4);
                                    textList.Add(gt4);
                                    listTT.Add(new ListThuocTinh { ID = cb4, Values = gt4 });
                                    break;
                            }
                            count++;
                        }
                    }
                    ttGroupService.AddThuocTinhByProduct(listTT, maGiuld.ToString());
                }
                MessageBox.Show("Thành công");
                btnDelete.Buttons[0].Enabled = true;
                btnEdit.Buttons[0].Enabled = true;
                grControl.DataBindings.Clear();
                grControl.DataSource = data;
            }
            else if (!string.IsNullOrEmpty(txtGiaBan.Text) && !string.IsNullOrEmpty(txtGiaNhap.Text) && txtGiaBan.Text.All(Char.IsNumber) == true && txtGiaNhap.Text.All(Char.IsNumber) == true&&Thaotac==null&&btnLuuTT.Enabled==false)
            {
                txtTen = txtTenHH.Text.Count();
                cbThuocTinh.Enabled = false;
                btnLuuTT.Enabled = true;
                HangHoa item1 = new HangHoa();
                item1.CheckVATBan = cbVATGB.Checked;
                item1.CheckVATMua = cbVATGV.Checked;
                item1.XuatXu = txtXuatXu.Text;
                item1.GhiChu = txtGhiChu.Text;
                item1.DonViTinh = cbDonViTinh.Text;
                if (!string.IsNullOrEmpty(urlImage))
                {
                    item1.ThumbnalImage = urlImage;
                }
                if (txtGiaBan.Text != null)
                    item1.GiaBan = double.Parse(txtGiaBan.Text);
                else
                    item1.GiaBan = 0;
                if (txtGiaNhap.Text != null)
                    item1.GiaMua = double.Parse(txtGiaNhap.Text);
                else
                    item1.GiaMua = 0;
                if (cbThuocTinh.Checked == true)
                {
                    item1.RootId = maGiuld.ToString();
                }
                else
                {
                    String MaHH = "";
                    using (var ctx = new KTVDbContext())
                    {
                        var ListMa = ctx.Database.SqlQuery<GetMa>("exec CreateMa 'HangHoas','MaHang'").FirstOrDefault();
                        if (ListMa == null)
                        {
                            MessageBox.Show("Sinh mã lỗi, thử lại sau.");
                        }
                        MaHH = ListMa.Ma;
                    }
                    item1.MaHang = MaHH;
                    //item1.MaHang = SinhMaTuDong(hangHoaService.CheckMax() + 1);
                }
                item1.TenHang = txtTenHH.Text;
                item1.CheckHome = cbHienThi.Checked;
                item1.NhaSanXuat = cbNhaSX.Text;
                if (cbDanhMuc.EditValue != null)
                {
                    item1.NhomHangId = int.Parse(cbDanhMuc.EditValue.ToString());
                }
                item1.Status = true;
                if (!string.IsNullOrEmpty(urlImage))
                {
                    item1.ThumbnalImage = urlImage;
                }
                if (txtTonKho.Text != null)
                    item1.SoLuong = double.Parse(txtTonKho.Text);
                else
                    item1.SoLuong = 0;
                var model = hangHoaService.Add(item1);
                ID = model.ID;
                for (int i = 0; i < grView.RowCount; i++)
                {
                    int index = grView.GetVisibleRowHandle(i);
                    HangHoa item = new HangHoa();
                    String MaHH = "";
                    using (var ctx = new KTVDbContext())
                    {
                        var ListMa = ctx.Database.SqlQuery<GetMa>("exec CreateMa 'HangHoas','MaHang'").FirstOrDefault();
                        if (ListMa == null)
                        {
                            MessageBox.Show("Sinh mã lỗi, thử lại sau.");
                        }
                        MaHH = ListMa.Ma;
                    }
                    item.DonViTinh = cbDonViTinh.Text;
                    item.MaHang = MaHH;
                    //int max = hangHoaService.CheckMax();
                    //item.MaHang = SinhMaTuDong(max + 1);
                    item.CheckVATBan = cbVATGB.Checked;
                    item.CheckVATMua = cbVATGV.Checked;
                    if (!string.IsNullOrEmpty(urlImage))
                    {
                        item.ThumbnalImage = urlImage;
                    }
                    item.GiaBan = double.Parse(grView.GetRowCellValue(index, "GiaBan").ToString());
                    item.GiaMua = double.Parse(grView.GetRowCellValue(index, "GiaMua").ToString());
                    item.RootId = maGiuld.ToString();
                    item.TenHang = grView.GetRowCellValue(index, "TenHang").ToString();
                    item.CheckHome = cbHienThi.Checked;
                    item.NhaSanXuat = cbNhaSX.Text;
                    if (cbDanhMuc.EditValue != null)
                    {
                        item.NhomHangId = int.Parse(cbDanhMuc.EditValue.ToString());
                    }
                    item.Status = true;
                    item.SoLuong = double.Parse(grView.GetRowCellValue(index, "SoLuong").ToString());
                    hangHoaService.Add(item);
                    data.Add(item);
                        newList.Add(new HangHoaVM
                        {
                            MaHang = item.MaHang,
                            TenHang = item.TenHang,
                            GiaMua = item.GiaMua,
                            GiaBan = item.GiaBan,
                            SoLuong = item.SoLuong
                        });
                
                }
                if (grView.RowCount > 0)
                {
                    int count = 1;
                    foreach (Control ctrl in groupControlTT.Controls)
                    {
                        if (ctrl is GridLookUpEdit)
                        {
                            switch (count)
                            {
                                case 1:
                                    cb1 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb1);
                                    textList.Add(giatri1);
                                    listTT.Add(new ListThuocTinh { ID = cb1, Values = giatri1 });
                                    break;
                                case 2:
                                    cb2 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb2);
                                    textList.Add(giatri2);
                                    listTT.Add(new ListThuocTinh { ID = cb2, Values = giatri2 });
                                    break;
                                case 3:
                                    cb3 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb3);
                                    textList.Add(giatri3);
                                    listTT.Add(new ListThuocTinh { ID = cb3, Values = giatri3 });
                                    break;
                                case 4:
                                    cb4 = thuocTinhService.GetByName(ctrl.Text).Id;
                                    cbList.Add(cb4);
                                    textList.Add(giatri4);
                                    listTT.Add(new ListThuocTinh { ID = cb4, Values = giatri4 });
                                    break;
                            }
                            count++;
                        }
                    }
                    ttGroupService.AddThuocTinhByProduct(listTT, maGiuld.ToString());
                }
                MessageBox.Show("Thành công");
                btnDelete.Buttons[0].Enabled = true;
                btnEdit.Buttons[0].Enabled = true;
                grControl.DataBindings.Clear();
                grControl.DataSource = data;               
                foreach (Control ctrl in groupControlTT.Controls)
                {
                    if (ctrl is ComboBoxEdit)
                    {
                        ctrl.Enabled = false;
                        txtGiaBan.Enabled = false;
                        txtGiaNhap.Enabled = false;
                    }
                }                
            }
           

        }

        private void btnThemNSX_Click(object sender, EventArgs e)
        {
            frmNhaSanXuat child = new frmNhaSanXuat();
            child.StartPosition = FormStartPosition.CenterScreen;
            child.FormClosed += new FormClosedEventHandler(FormClose);
            child.ShowDialog();
        }

        private void btnAddTT_Click(object sender, EventArgs e)
        {
            int xcb = 12;
            int ycb = 12;
            int xtag = 168;
            int ytag = 12;
            int count = groupControlTT.Controls.OfType<GridLookUpEdit>().Count();
            if (count < 5)
            {
                int count1 = groupControlTT.Controls.OfType<TextBox>().Count();
                GridLookUpEdit comboBoxEdit = new GridLookUpEdit();
                comboBoxEdit.Name = "cb" + count + 1;
                comboBoxEdit.Properties.DataSource = LoadDL2();
                comboBoxEdit.Properties.ValueMember = "ID";
                comboBoxEdit.Properties.DisplayMember = "Ten";
                comboBoxEdit.Text = LoadDL2().Rows[LoadDL2().Rows.Count - 1 - count][0].ToString();
                comboBoxEdit.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                comboBoxEdit.Properties.ImmediatePopup = true;
                comboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                comboBoxEdit.Location = new System.Drawing.Point(xcb, ycb + 30 * (count + 1));
                comboBoxEdit.MinimumSize = new Size(152, 25);
                comboBoxEdit.Size = new System.Drawing.Size(152, 25);
                groupControlTT.Controls.Add(comboBoxEdit);
                ycb += 30;
                TextBox tagListControl = new TextBox();
                tagListControl.Name = "taglist" + count1 + 1;
                tagListControl.Location = new System.Drawing.Point(xtag, ytag + 30 * (count + 1));
                tagListControl.MinimumSize = new Size(1000, 25);
                tagListControl.Size = new System.Drawing.Size(1000, 25);
                tagListControl.KeyPress += new KeyPressEventHandler(text_KeyPress);
                groupControlTT.Controls.Add(tagListControl);
            }
           
        }

        private void frmEditHangHoa_Load(object sender, EventArgs e)
        {
            maGiuld = Guid.NewGuid();
            LoadDL();
            LoadData();
            LoadDL3();
            cbDonViTinh.Text = "cái";
            if (Thaotac == "Edit")
            {
                var model = hangHoaService.GetById(ID);
                txtTen = model.TenHang.Count();
                if (model.NhomHangId > 0)
                {
                    cbDanhMuc.EditValue = model.NhomHangId;
                }
                btnAddTT.Enabled = false;
                simpleButton5.Enabled = false;
                txtTenHH.Text = model.TenHang;
                cbVATGB.Checked = model.CheckVATBan;
                cbVATGV.Checked = model.CheckVATMua;
                cbNhaSX.Text = model.NhaSanXuat;
                txtGiaBan.Enabled = true;
                txtGiaNhap.Enabled = false;
                txtPhanTram.Text = ((model.GiaBan / model.GiaMua - 1) * 100).ToString();
                txtGiaNhap.Text = model.GiaMua.ToString();
                txtGiaBan.Text = model.GiaBan.ToString();               
                txtMaHH.Text = model.MaHang;
                txtGhiChu.Text = model.GhiChu;
                txtXuatXu.Text = model.XuatXu;
                cbDonViTinh.Text = model.DonViTinh;
                txtTonKho.Text = model.SoLuong.ToString();
                if (model.RootId != null)
                {
                    txtPhanTram.Enabled = false;
                    txtGiaBan.Enabled = false;
                    maGiuld =Guid.Parse(model.RootId);
                    txtMaHH.Enabled = false;
                    txtTonKho.Enabled = false;
                    cbThuocTinh.Checked = true;
                    cbThuocTinh.Enabled = false;
                    btnLuuTT.Enabled = true;
                    layoutControl3.Visible = true;
                    layoutControl4.Visible = true;
                    layoutControl2.Visible = true;
                    grView.Columns["SoLuong"].OptionsColumn.AllowEdit = false;
                    var modelTT = ttGroupService.GetByRootId(model.RootId);
                    for(int i = 0; i < modelTT.Count; i++)
                    {
                        int xcb = 12;
                        int ycb = 12;
                        int xtag = 168;
                        int ytag = 12;
                        GridLookUpEdit comboBoxEdit = new GridLookUpEdit();
                        comboBoxEdit.Name = "cb" + i + 1;
                        comboBoxEdit.Properties.DataSource = LoadDL2();
                        comboBoxEdit.Properties.ValueMember = "ID";
                        comboBoxEdit.Properties.DisplayMember = "Ten";
                        var tt = thuocTinhService.GetById(modelTT.ElementAt(i).ThuocTinhId);
                        comboBoxEdit.EditValue = tt.Id;
                        comboBoxEdit.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                        comboBoxEdit.Properties.ImmediatePopup = true;
                        comboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                        comboBoxEdit.Location = new System.Drawing.Point(xcb, ycb + 30 * (i + 1));
                        comboBoxEdit.MinimumSize = new Size(152, 25);
                        comboBoxEdit.Size = new System.Drawing.Size(152, 25);
                        groupControlTT.Controls.Add(comboBoxEdit);
                        ycb += 30;
                        TextBox tagListControl = new TextBox();
                        tagListControl.Name = "taglist" + i + 1;
                        tagListControl.Text = modelTT.ElementAt(i).Values;
                                
                        tagListControl.Location = new System.Drawing.Point(xtag, ytag + 30 * (i + 1));
                        tagListControl.MinimumSize = new Size(1000, 25);
                        tagListControl.Size = new System.Drawing.Size(1000, 25);
                        tagListControl.KeyPress += new KeyPressEventHandler(text_KeyPress);
                        groupControlTT.Controls.Add(tagListControl);
                    }
                    var listhh = hangHoaService.GetListByRootId(model.RootId);
                    grControl.DataBindings.Clear();
                    grControl.DataSource = listhh.ToList();
                    btnEdit.Buttons[0].Enabled = true;
                    foreach(var item in listhh)
                    {
                        newList.Add(new HangHoaVM
                        {
                            MaHang = item.MaHang,
                            TenHang = item.TenHang,
                            GiaMua = item.GiaMua,
                            GiaBan = item.GiaBan,
                            SoLuong = item.SoLuong
                        });
                        data.Add(new HangHoa
                        {
                            MaHang = item.MaHang,
                            TenHang = item.TenHang,
                            GiaMua = item.GiaMua,
                            GiaBan = item.GiaBan,
                            SoLuong = item.SoLuong
                        });
                    }
                }
                else
                {
                    cbThuocTinh.Enabled = false;
                    layoutControl3.Visible = false;
                    layoutControl4.Visible = false;
                    layoutControl2.Visible = false;
                    btnLuuTT.Enabled = true;
                }
                if (model.ThumbnalImage != null)
                {
                   string paths = "\\\\" + IPMAYCHU + "\\D$\\Images\\" + model.ThumbnalImage;
                   //string paths = System.IO.Path.GetFullPath(@"\Images\" + model.ThumbnalImage);
                        //Application.StartupPath.Substring(2, (Application.StartupPath.Length - 11));
                    imageHH.Image = Image.FromFile(paths);
                    imageHH.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
                }              
            }
            if (Thaotac == "Copy")
            {
                btnLuuTT.Enabled = false;
                var model = hangHoaService.GetById(ID);
                maGiuld = Guid.NewGuid();
                btnLuuTT.Enabled = false;
                if (model.RootId != null)
                {
                    txtGhiChu.Text = model.GhiChu;
                    txtXuatXu.Text = model.XuatXu;
                    if (model.NhomHangId > 0)
                    {
                        cbDanhMuc.EditValue = model.NhomHangId;
                    }
                    int count = hangHoaService.CheckExitTenSP(model.TenHang);
                    if (count==1)
                    {
                        txtTenHH.Text =model.TenHang+" Copy";
                    }
                    else
                        txtTenHH.Text = model.TenHang + " Copy "+count;
                    txtTen = txtTenHH.Text.Count();
                    cbVATGB.Checked = model.CheckVATBan;
                    cbVATGV.Checked = model.CheckVATMua;
                    cbNhaSX.Text = model.NhaSanXuat;
                    txtGiaNhap.Text = model.GiaMua.ToString();
                    txtGiaBan.Text = model.GiaBan.ToString();
                    if (model.GiaBan > 0 && model.GiaMua > 0)
                    {
                        txtPhanTram.Text = ((model.GiaBan / model.GiaMua - 1)*100).ToString();
                    }
                    else
                        txtPhanTram.Text = "0";
                    cbThuocTinh.Checked = true;
                    cbThuocTinh.Enabled = false;
                    btnLuuTT.Enabled = true;
                    layoutControl3.Visible = true;
                    layoutControl4.Visible = true;
                    layoutControl2.Visible = true;
                    var modelTT = ttGroupService.GetByRootId(model.RootId);
                    countControl = modelTT.Count;
                    for (int i = 0; i < modelTT.Count; i++)
                    {
                        int xcb = 12;
                        int ycb = 12;
                        int xtag = 168;
                        int ytag = 12;
                        GridLookUpEdit comboBoxEdit = new GridLookUpEdit();
                        comboBoxEdit.Name = "cb" + i + 1;
                        comboBoxEdit.Properties.DataSource = LoadDL2();
                        comboBoxEdit.Properties.ValueMember = "ID";
                        comboBoxEdit.Properties.DisplayMember = "Ten";
                        var tt = thuocTinhService.GetById(modelTT.ElementAt(i).ThuocTinhId);
                        comboBoxEdit.EditValue = tt.Id;
                        comboBoxEdit.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                        comboBoxEdit.Properties.ImmediatePopup = true;
                        comboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                        comboBoxEdit.Location = new System.Drawing.Point(xcb, ycb + 30 * (i + 1));
                        comboBoxEdit.MinimumSize = new Size(152, 25);
                        comboBoxEdit.Size = new System.Drawing.Size(152, 25);
                        groupControlTT.Controls.Add(comboBoxEdit);
                        ycb += 30;
                        TextBox tagListControl = new TextBox();
                        tagListControl.Name = "taglist" + i + 1;
                        tagListControl.Text = modelTT.ElementAt(i).Values;
                        giatri1 = modelTT.ElementAt(0).Values;
                        gt1 = modelTT.ElementAt(0).Values;
                        if (modelTT.Count>1)
                        {
                            giatri2 = modelTT.ElementAt(1).Values;
                            gt2 = modelTT.ElementAt(1).Values;
                        }                           
                        if (modelTT.Count >2)
                        {
                            giatri3 = modelTT.ElementAt(2).Values;
                            gt3 = modelTT.ElementAt(2).Values;

                        }
                        if (modelTT.Count > 3)
                        {
                            giatri4 = modelTT.ElementAt(3).Values;
                            gt4 = modelTT.ElementAt(3).Values;
                        }                           
                        tagListControl.Location = new System.Drawing.Point(xtag, ytag + 30 * (i + 1));
                        tagListControl.MinimumSize = new Size(1000, 25);
                        tagListControl.Size = new System.Drawing.Size(1000, 25);
                        tagListControl.KeyPress += new KeyPressEventHandler(text_KeyPress);
                        groupControlTT.Controls.Add(tagListControl);
                    }
                    var listhh = hangHoaService.GetListByRootId(model.RootId);
                    for(int m = 0; m < listhh.Count; m++)
                    {
                        data1.Add(new HangHoa
                        {
                            MaHang = "",
                            TenHang = listhh[m].TenHang.Replace(model.TenHang, txtTenHH.Text),
                            GiaMua = listhh[m].GiaMua,
                            GiaBan = listhh[m].GiaBan,
                            SoLuong = 0
                        });
                    }
                    grControl.DataBindings.Clear();
                    grControl.DataSource = data1;
                    btnEdit.Buttons[0].Enabled = false;
                    btnDelete.Buttons[0].Enabled = false;
                }
                else
                {

                    if (model.NhomHangId > 0)
                    {
                        cbDanhMuc.EditValue = model.NhomHangId;
                    }
                    int count = hangHoaService.CheckExitTenSP(model.TenHang);
                    if (count == 1)
                    {
                        txtTenHH.Text = model.TenHang + " Copy";
                    }
                    else
                        txtTenHH.Text = model.TenHang + " Copy " + count;
                    txtTen = txtTenHH.Text.Count();
                    cbVATGB.Checked = model.CheckVATBan;
                    cbVATGV.Checked = model.CheckVATMua;
                    cbNhaSX.Text = model.NhaSanXuat;
                    txtGiaNhap.Text = model.GiaMua.ToString();
                    txtGiaBan.Text = model.GiaBan.ToString();
                    txtGhiChu.Text = model.GhiChu;
                    txtXuatXu.Text = model.XuatXu;
                    if (model.GiaBan > 0 && model.GiaMua > 0)
                    {
                        txtPhanTram.Text = ((model.GiaBan / model.GiaMua - 1) * 100).ToString();
                    }
                    else
                        txtPhanTram.Text = "0";
                    cbThuocTinh.Checked = false;
                    cbThuocTinh.Enabled = false;
                    btnLuuTT.Enabled = false;
                    layoutControl3.Visible = true;
                    layoutControl4.Visible = false;
                    layoutControl2.Visible = false;
                }
            }
        }

        private void btnAddDM_Click(object sender, EventArgs e)
        {
            frmDanhMucHH hH = new frmDanhMucHH();
            hH.FormClosed += new FormClosedEventHandler(FormClose1);
            hH.ShowDialog();
        }

        public void LoadDL()
        {
            var model = nhaSXService.GetAll();
            cbNhaSX.Properties.Items.Clear();
            for (int i = model.Count - 1; i > -1; i--)
            {
                cbNhaSX.Properties.Items.Add(model[i].Name);
            }
        }
        public DataTable LoadDL2()
        {
            var model = thuocTinhService.GetAll();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[2] { new DataColumn("ID"), new DataColumn("Ten") });
            for (int i = model.Count - 1; i > -1; i--)
            {
                dt.Rows.Add(model[i].Id, model[i].Name);
            }
            return dt;
        }
        public void LoadDL3()
        {
            var model = donViTinhService.GetAll();
            cbDonViTinh.Properties.Items.Clear();
            for (int i = model.Count - 1; i > -1; i--)
            {
                cbDonViTinh.Text = model[model.Count - 1].Name;
                cbDonViTinh.Properties.Items.Add(model[i].Name);
            }
        }
        public void LoadData()
        {
            var listmh = nhomHHService.GetAll();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[2] { new DataColumn("ID"), new DataColumn("Ten") });
            foreach (var item in listmh)
            {
                dt.Rows.Add(item.Id, item.Name);
            }
            //cbDanhMuc.Text = listmh[listmh.Count - 1].Name;
            cbDanhMuc.Properties.DataSource = dt;
            cbDanhMuc.Properties.ValueMember = "ID";
            cbDanhMuc.Properties.DisplayMember = "Ten";
            cbDanhMuc.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
            cbDanhMuc.Properties.ImmediatePopup = true;
            cbDanhMuc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            frmThuocTinhHH tt = new frmThuocTinhHH();
            tt.FormClosed += new FormClosedEventHandler(FormClose3);
            tt.ShowDialog();
        }

        private void FormClose(object sender, FormClosedEventArgs e)
        {
            LoadDL();
            this.Show();
        }

        private void btnEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                if(grView.GetRowCellValue(index, grView.Columns[1]).ToString() != "")
                {
                    if (double.Parse(grView.GetRowCellValue(index, grView.Columns[3]).ToString()) > double.Parse(grView.GetRowCellValue(index, grView.Columns[2]).ToString()))
                    {
                        var model = hangHoaService.GetByMaHang(grView.GetRowCellValue(index, grView.Columns[1]).ToString());
                        model.GiaBan = double.Parse(grView.GetRowCellValue(index, grView.Columns[3]).ToString());
                        DialogResult dr =  XtraMessageBox.Show("Bạn có muốn thay đổi giá sản phẩm này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.OK)
                        {
                            hangHoaService.Update(model);
                            MessageBox.Show("Cập nhật thành công");
                        }                        
                    }
                    else
                    {
                        MessageBox.Show("Giá bán không được nhỏ hơn giá mua");
                    }
                }
                else
                {
                    MessageBox.Show("Sản phẩm chưa có trong hệ thống");
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        private void grView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            index = e.FocusedRowHandle;
        }
        private void btnThemDVT_Click(object sender, EventArgs e)
        {
            frmDonViTinh donViTinh = new frmDonViTinh();
            donViTinh.FormClosed += new FormClosedEventHandler(FormClose4);
            donViTinh.ShowDialog();
        }

        private void FormClose1(object sender, FormClosedEventArgs e)
        {
            LoadData();
            this.Show();
        }

        private void cbDanhMuc_TextChanged(object sender, EventArgs e)
        {
            if (cbDanhMuc.Text == null)
            {
                cbDanhMuc.EditValue = null;
            }
        }

        private void txtGiaBan_TextChanged(object sender, EventArgs e)
        {
            if(int.Parse(txtGiaBan.Text)>0&& int.Parse(txtGiaNhap.Text)>0)
            {
                txtPhanTram.Text = ((int.Parse(txtGiaBan.Text) / int.Parse(txtGiaNhap.Text) - 1) * 100).ToString();
            }           
            for (int i = 0; i < grView.RowCount; i++)
            {
                int row = grView.GetVisibleRowHandle(i);
                grView.SetRowCellValue(row, "GiaMua", txtGiaNhap.Text);
                grView.SetRowCellValue(row, "GiaBan", txtGiaBan.Text);
            }
        }

        private void btnDelete_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            try
            {
                if (grView.GetRowCellValue(index, grView.Columns[1]).ToString() != "")
                {                    
                    var model = hangHoaService.GetByMaHang(grView.GetRowCellValue(index, grView.Columns[1]).ToString());
                    model.Status = false;
                    DialogResult dr = XtraMessageBox.Show("Bạn có muốn ngừng kinh doanh sản phẩm này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.OK)
                    {
                        hangHoaService.Update(model);
                        MessageBox.Show("Cập nhật thành công");
                    }                                    
                }
                else
                {
                    MessageBox.Show("Sản phẩm chưa có trong hệ thống");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                SearchImage();
                // Hỏi trước khi lưu ảnh                    
                SaveImage();
            }
            catch
            {
            }
        }
        private void SearchImage()
        {
            fileOpen.InitialDirectory = "D:";//Định đường dẫn khi mở cửa sổ tìm ảnh
            fileOpen.FileName = "";// Tên ảnh
            fileOpen.Filter = "Images(*.jpg)|*.jpg|PNG (*.png)|*.png|All files (*.*)|*.*";
            fileOpen.ShowDialog();// Hiện cửa sổ 
            if (fileOpen.FileName != "")
            {

                urlImage = System.IO.Path.GetFullPath(fileOpen.FileName);// Hiển thị tên ảnh lên Textbox
                imageHH.Image = Image.FromFile(fileOpen.FileName);//Hiện ảnh lên Picbox.
                imageHH.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
                System.IO.Directory.SetCurrentDirectory(@"D:/Images");//Đường dẫn chứa file ảnh khi lưu.
            }
        }
        private void SaveImage()
        {
            fileSave.FileName = urlImage;

            if (fileSave.FileName != "")
            {

                System.IO.FileStream fs = (System.IO.FileStream)fileSave.OpenFile();
                switch (fileSave.FilterIndex)
                {

                    case 1:

                        this.imageHH.Image.Save(fs,
                        System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;

                    case 2:
                        this.imageHH.Image.Save(fs,
                        System.Drawing.Imaging.ImageFormat.Bmp);
                        break;
                    case 3:
                        this.imageHH.Image.Save(fs,
                        System.Drawing.Imaging.ImageFormat.Gif);
                        break;
                }
                fs.Close();
            }
        }

        private void btnThemHH_Click(object sender, EventArgs e)
        {
            Thaotac = "";
            maGiuld = Guid.NewGuid();
            EnableForm();
        }
        public void EnableForm()
        {
            cbThuocTinh.Enabled = true;
            cbDanhMuc.EditValue = null;
            data.Clear();
            newList.Clear();
            if (list1 != null)
                list1.Clear();
            if (list2 != null)
                list2.Clear();
            if (list3 != null)
                list3.Clear();
            if (list4 != null)
                list4.Clear();
            giatri1 = "";
            giatri2 = "";
            giatri3 = "";
            giatri4 = "";
            cbCheckVAT.Checked = false;
            cbDonViTinh.Text = "cái";
            cbHienThi.Checked = false;
            cbThuocTinh.Checked = false;
            cbVATGB.Checked = false;
            cbVATGV.Checked = false;
            txtGiaBan.Enabled = true;
            txtGiaNhap.Enabled = true;
            txtGiaBan.Text = "0";
            txtGiaNhap.Text = "0";
            txtMaHH.Text = "";
            txtPhanTram.Text = "0";
            txtTenHH.Text = "";
            txtTonKho.Text = "0";
            txtTonKho.Enabled = true;
            txtMaHH.Enabled = true;
            btnLuuTT.Enabled = false;
            grControl.DataBindings.Clear();
            grControl.DataSource = null;
            layoutControl3.Visible = false;
            layoutControl4.Visible = false;
            layoutControl2.Visible = false;
            btnDelete.Buttons[0].Enabled = false;
            btnEdit.Buttons[0].Enabled = false;
            foreach (Control ctrl in groupControlTT.Controls)
            {
                if (ctrl is ComboBoxEdit)
                {
                    ctrl.ResetText();
                    ctrl.Enabled = true;
                    this.Controls.Remove(ctrl);
                }
                if (ctrl is TextBox)
                {
                    ctrl.ResetText();
                    this.Controls.Remove(ctrl);
                }
            }
        }

        private void txtTonKho_TextChanged(object sender, EventArgs e)
        {
            txtTen = txtTenHH.Text.Count();
        }

        private void grView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            double tong = 0;
            for (int i = 0; i < grView.RowCount; i++)
            {
                if (e.Column == grView.Columns["SoLuong"])
                {
                    int row = grView.GetVisibleRowHandle(i);
                    tong = tong + double.Parse(grView.GetRowCellValue(row, "SoLuong").ToString());
                }
            }
            var model = hangHoaService.GetById(ID);
            if (model != null)
            {
                model.SoLuong = tong;
                hangHoaService.Update(model);
                txtTonKho.Text = tong.ToString();
            }          
        }

        private void FormClose4(object sender, FormClosedEventArgs e)
        {
            LoadDL3();
            this.Show();
        }
        private void txtTenHH_TextChanged(object sender, EventArgs e)
        {          
                if (!string.IsNullOrEmpty(txtTenHH.Text) && txtTenHH.Text.Count() < txtTen && cbThuocTinh.Checked == true)
                {
                    for (int i = 0; i < grView.RowCount; i++)
                    {
                        int row = grView.GetVisibleRowHandle(i);
                        string chuoicon = grView.GetRowCellValue(row, "TenHang").ToString().Substring(0, txtTen);
                        string chuoi = grView.GetRowCellValue(row, "TenHang").ToString().Replace(chuoicon, txtTenHH.Text);
                        grView.SetRowCellValue(row, "TenHang", chuoi);
                    }
                    txtTen = txtTen - 1;
                }
                else if (!string.IsNullOrEmpty(txtTenHH.Text) && txtTenHH.Text.Count() > txtTen && cbThuocTinh.Checked == true)
                {
                    for (int i = 0; i < grView.RowCount; i++)
                    {
                        int row = grView.GetVisibleRowHandle(i);
                        string chuoicon = grView.GetRowCellValue(row, "TenHang").ToString().Substring(0, txtTen);
                        string chuoi = grView.GetRowCellValue(row, "TenHang").ToString().Replace(chuoicon, txtTenHH.Text);
                        grView.SetRowCellValue(row, "TenHang", chuoi);
                    }
                    txtTen = txtTen + 1;
                }
                if (!string.IsNullOrEmpty(txtTenHH.Text))
                {
                    layoutControl3.Visible = true;
                }           
        }

        private void btnLuuTT_Click(object sender, EventArgs e)
        {
            if (txtMaHH.Text != null && txtMaHH.Text != "Mã hàng hóa")
            {
                var item1 = hangHoaService.GetById(ID);
                item1.CheckVATBan = cbVATGB.Checked;
                item1.CheckVATMua = cbVATGV.Checked;
                item1.XuatXu = txtXuatXu.Text;
                item1.GhiChu = txtGhiChu.Text;
                item1.DonViTinh = cbDonViTinh.Text;
                if (!string.IsNullOrEmpty(urlImage))
                {
                    item1.ThumbnalImage = urlImage;
                }
                if (txtGiaBan.Text != null)
                    item1.GiaBan = double.Parse(txtGiaBan.Text);
                if (txtGiaNhap.Text != null)
                    item1.GiaMua = double.Parse(txtGiaNhap.Text);
                item1.TenHang = txtTenHH.Text;
                item1.CheckHome = cbHienThi.Checked;
                item1.NhaSanXuat = cbNhaSX.Text;
                if (cbDanhMuc.EditValue != null)
                {
                    item1.NhomHangId = int.Parse(cbDanhMuc.EditValue.ToString());
                }
                item1.Status = true;
                if (!string.IsNullOrEmpty(urlImage))
                {
                    item1.ThumbnalImage = urlImage;
                }
                hangHoaService.Update(item1);
                MessageBox.Show("Cập nhật thành công");
            }
            if (grView.RowCount > 0&&Thaotac=="Edit")
            {
                var item1 = hangHoaService.GetById(ID);
                item1.CheckVATBan = cbVATGB.Checked;
                item1.CheckVATMua = cbVATGV.Checked;
                item1.XuatXu = txtXuatXu.Text;
                item1.GhiChu = txtGhiChu.Text;
                item1.DonViTinh = cbDonViTinh.Text;
                if (!string.IsNullOrEmpty(urlImage))
                {
                    item1.ThumbnalImage = urlImage;
                }
                if (txtGiaBan.Text !="")
                    item1.GiaBan = double.Parse(txtGiaBan.Text);
                if (txtGiaNhap.Text !="")
                    item1.GiaMua = double.Parse(txtGiaNhap.Text);
                item1.TenHang = txtTenHH.Text;
                item1.CheckHome = cbHienThi.Checked;
                item1.NhaSanXuat = cbNhaSX.Text;
                if (cbDanhMuc.EditValue != null)
                {
                    item1.NhomHangId = int.Parse(cbDanhMuc.EditValue.ToString());
                }
                item1.Status = true;
                if (!string.IsNullOrEmpty(urlImage))
                {
                    item1.ThumbnalImage = urlImage;
                }
                hangHoaService.Update(item1);
                for (int i = 0; i < grView.RowCount; i++)
                {
                    int index = grView.GetVisibleRowHandle(i);
                    var item = hangHoaService.GetById(int.Parse(grView.GetRowCellValue(index, "ID").ToString()));
                    item.TenHang = grView.GetRowCellValue(index, "TenHang").ToString();
                    hangHoaService.Update(item);
                }
                MessageBox.Show("Cập nhật thành công");
            }
            else
            {
                var item1 = hangHoaService.GetById(ID);
                item1.CheckVATBan = cbVATGB.Checked;
                item1.CheckVATMua = cbVATGV.Checked;
                item1.XuatXu = txtXuatXu.Text;
                item1.GhiChu = txtGhiChu.Text;
                item1.DonViTinh = cbDonViTinh.Text;
                if (!string.IsNullOrEmpty(urlImage))
                {
                    item1.ThumbnalImage = urlImage;
                }
                if (txtGiaBan.Text != null)
                    item1.GiaBan = double.Parse(txtGiaBan.Text);
                if (txtGiaNhap.Text != null)
                    item1.GiaMua = double.Parse(txtGiaNhap.Text);
                item1.TenHang = txtTenHH.Text;
                item1.CheckHome = cbHienThi.Checked;
                item1.NhaSanXuat = cbNhaSX.Text;
                if (cbDanhMuc.EditValue != null)
                {
                    item1.NhomHangId = int.Parse(cbDanhMuc.EditValue.ToString());
                }
                item1.Status = true;
                if (!string.IsNullOrEmpty(urlImage))
                {
                    item1.ThumbnalImage = urlImage;
                }
                hangHoaService.Update(item1);
                for (int i = 0; i < grView.RowCount; i++)
                {
                    int index = grView.GetVisibleRowHandle(i);
                    var item = hangHoaService.GetById(int.Parse(grView.GetRowCellValue(index, "ID").ToString()));
                    item.TenHang = grView.GetRowCellValue(index, "TenHang").ToString();
                    hangHoaService.Update(item);
                }
                MessageBox.Show("Cập nhật thành công");
            }
        }

        private void FormClose3(object sender, FormClosedEventArgs e)
        {
            LoadDL2();
        }
        public void text_KeyPress(object sender, KeyPressEventArgs e)
        {
            List<HangHoaVM> listHH = new List<HangHoaVM>();
            grControl.DataBindings.Clear();
            int count = 1;
            int dem = 1;
            int dem1 = 1;
            int dem2 = 1;
            int dem3 = 1;
            int dem4 = 1;
            int tt1, tt2, tt3, tt4;
            tt1 = tt2 = tt3 = tt4 = 1;
            if (e.KeyChar == 13)
            {
                foreach (Control ctrl in groupControlTT.Controls)
                {
                    string ten = ctrl.Name;
                    if (ctrl is TextBox && btnLuuTT.Enabled == false)
                    {
                        string chuoi = ctrl.Text;
                        switch (count)
                        {
                            case 1:
                                giatri1 = chuoi;
                                list1 = chuoi.Split(';').ToList();
                                break;
                            case 2:
                                giatri2 = chuoi;
                                list2 = chuoi.Split(';').ToList();
                                break;
                            case 3:
                                giatri3 = chuoi;
                                list3 = chuoi.Split(';').ToList();
                                break;
                            case 4:
                                giatri4 = chuoi;
                                list4 = chuoi.Split(';').ToList();
                                break;
                        }
                        count++;
                    }
                    if (ctrl is TextBox && btnLuuTT.Enabled == true&&Thaotac!="Copy")
                    {
                        string chuoi = ctrl.Text;
                        switch (count)
                        {
                            case 1:
                                if (String.Compare(giatri1, chuoi, true) > 0 && !string.IsNullOrEmpty(giatri1))
                                {
                                    list1 = giatri1.Split(';').ToList();
                                    gt1 = giatri1;
                                }
                                else
                                    gt1 = chuoi;
                                list1 = chuoi.Split(';').ToList();
                                break;
                            case 2:
                                if (String.Compare(giatri2, chuoi, true) > 0 && !string.IsNullOrEmpty(giatri2))
                                {
                                    list2 = giatri2.Split(';').ToList();
                                    gt2 = giatri2;
                                }
                                else
                                    gt2 = chuoi;
                                list2 = chuoi.Split(';').ToList();
                                break;
                            case 3:
                                if (String.Compare(giatri3, chuoi, true) > 0 && !string.IsNullOrEmpty(giatri3))
                                {
                                    list3 = giatri3.Split(';').ToList();
                                    gt3 = giatri3;
                                }
                                else
                                    gt3 = chuoi;
                                list3 = chuoi.Split(';').ToList();
                                break;
                            case 4:
                                if (String.Compare(giatri4, chuoi, true) > 0 && !string.IsNullOrEmpty(giatri4))
                                {
                                    list4 = giatri4.Split(';').ToList();
                                    gt4 = giatri4;
                                }
                                else
                                    gt4 = chuoi;
                                list4 = chuoi.Split(';').ToList();
                                break;
                        }
                        count++;
                    }
                    if (ctrl is TextBox && btnLuuTT.Enabled == true && Thaotac=="Copy")
                    {

                        string chuoi = ctrl.Text;
                        switch (count)
                        {
                            case 1:
                                if (String.Compare(giatri1, chuoi, true) > 0 && !string.IsNullOrEmpty(giatri1))
                                {
                                    list1 = giatri1.Split(';').ToList();
                                    gt1 = giatri1;
                                }
                                else
                                    gt1 = chuoi;
                                list1 = chuoi.Split(';').ToList();
                                break;
                            case 2:
                                if (String.Compare(giatri2, chuoi, true) > 0 && !string.IsNullOrEmpty(giatri2))
                                {
                                    list2 = giatri2.Split(';').ToList();
                                    gt2 = giatri2;
                                }
                                else
                                    gt2 = chuoi;
                                list2 = chuoi.Split(';').ToList();
                                break;
                            case 3:
                                if (String.Compare(giatri3, chuoi, true) > 0 && !string.IsNullOrEmpty(giatri3))
                                {
                                    list3 = giatri3.Split(';').ToList();
                                    gt3 = giatri3;
                                }
                                else
                                    gt3 = chuoi;
                                list3 = chuoi.Split(';').ToList();
                                break;
                            case 4:
                                if (String.Compare(giatri4, chuoi, true) > 0 && !string.IsNullOrEmpty(giatri4))
                                {
                                    list4 = giatri4.Split(';').ToList();
                                    gt4 = giatri4;
                                }
                                else
                                    gt4 = chuoi;
                                list4 = chuoi.Split(';').ToList();
                                break;
                        }
                        count++;
                    }                  
                }
                if(countControl!= groupControlTT.Controls.OfType<GridLookUpEdit>().Count()&&Thaotac=="Copy")
                    newList.Clear();

                if (list1 != null)
                    dem1 = list1.Count;
                else
                    tt1 = 0;
                if (list2 != null)
                    dem2 = list2.Count;
                else
                    tt2 = 0;
                if (list3 != null)
                    dem3 = list3.Count;
                else
                    tt3 = 0;
                if (list4 != null)
                    dem4 = list4.Count;
                else
                    tt4 = 0;
                dem = dem1 * dem2 * dem3 * dem4;
                for (int i = 0; i < dem; i++)
                {
                    string ten = txtTenHH.Text;
                    if (tt1 != 0)
                    {
                        ten = ten + " " + list1[(i % (dem1 * dem2 * dem3 * dem4)) / (dem2 * dem3 * dem4)];
                    }
                    if (tt2 != 0)
                    {
                        ten = ten + " " + list2[(i % (dem2 * dem3 * dem4)) / (dem3 * dem4)];
                    }
                    if (tt3 != 0)
                    {
                        ten = ten + " " + list3[(i % (dem3 * dem4)) / dem4];
                    }
                    if (tt4 != 0)
                    {
                        ten = ten + " " + list4[(i % dem4 * 1) / 1];
                    }
                    ten = ten + " ";                   
                    if (btnLuuTT.Enabled == false)
                    {
                        listHH.Add(new HangHoaVM
                        {
                            MaHang = "",
                            TenHang = ten,
                            GiaMua = float.Parse(txtGiaNhap.Text),
                            GiaBan = float.Parse(txtGiaBan.Text),
                            SoLuong = 0
                        });
                    }                    
                    else if (!newList.Exists(x => x.TenHang.Trim() == ten.Trim()) && btnLuuTT.Enabled == true)
                    {
                        newList.Add(new HangHoaVM
                        {
                            MaHang = "",
                            TenHang = ten,
                            GiaMua = float.Parse(txtGiaNhap.Text),
                            GiaBan = float.Parse(txtGiaBan.Text),
                            SoLuong = 0
                        });
                        grView.Columns["SoLuong"].OptionsColumn.AllowEdit = true;
                    }
                    
                }
                if (btnLuuTT.Enabled == true)
                {
                    grControl.DataBindings.Clear();
                    grControl.DataSource = null;
                    grControl.DataSource = newList;
                    var model = hangHoaService.GetById(ID);
                    model.SoLuong = newList.Sum(x => x.SoLuong);
                    hangHoaService.Update(model);
                }
                if (btnLuuTT.Enabled == false)
                {
                    grControl.DataBindings.Clear();                   
                    grControl.DataSource = listHH;
                    btnDelete.Buttons[0].Enabled = false;
                    btnEdit.Buttons[0].Enabled = false;
                }
            }
        }
        public string SinhMaTuDong(int ma)
        {
            string chuoi = null;
            if (ma < 10)
                chuoi = "SP00000" + ma;
            if (ma < 100)
                chuoi = "SP0000" + ma;
            if (ma >= 100 && ma < 1000)
                chuoi = "SP000" + ma;
            if (ma >= 1000 && ma < 10000)
                chuoi = "SP00" + ma;
            if (ma >= 10000 && ma < 100000)
                chuoi = "SP0" + ma;
            if (ma >= 100000)
                chuoi = "SP" + ma;
            return chuoi;
        }
    }
}