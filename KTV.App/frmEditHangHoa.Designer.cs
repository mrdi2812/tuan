﻿namespace KTV.App
{
    partial class frmEditHangHoa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditHangHoa));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtGhiChu = new System.Windows.Forms.RichTextBox();
            this.txtXuatXu = new DevExpress.XtraEditors.TextEdit();
            this.cbDanhMuc = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.imageHH = new DevExpress.XtraEditors.PictureEdit();
            this.btnUpload = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemDVT = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemHH = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuTT = new DevExpress.XtraEditors.SimpleButton();
            this.cbNhaSX = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbThuocTinh = new DevExpress.XtraEditors.CheckButton();
            this.txtPhanTram = new DevExpress.XtraEditors.TextEdit();
            this.cbVATGB = new DevExpress.XtraEditors.CheckEdit();
            this.txtGiaBan = new DevExpress.XtraEditors.TextEdit();
            this.cbVATGV = new DevExpress.XtraEditors.CheckEdit();
            this.txtGiaNhap = new DevExpress.XtraEditors.TextEdit();
            this.cbCheckVAT = new DevExpress.XtraEditors.CheckEdit();
            this.cbHienThi = new DevExpress.XtraEditors.CheckEdit();
            this.txtTonKho = new DevExpress.XtraEditors.TextEdit();
            this.btnThemNSX = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddDM = new DevExpress.XtraEditors.SimpleButton();
            this.cbDonViTinh = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtMaHH = new DevExpress.XtraEditors.TextEdit();
            this.txtTenHH = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.groupControlTT = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddTT = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.btnHuy = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.grControl = new DevExpress.XtraGrid.GridControl();
            this.grView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TenHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GiaVon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GiaBan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoLuong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Edit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.Delete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDelete = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.cbNhaCC = new DevExpress.XtraEditors.ComboBoxEdit();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.label1 = new System.Windows.Forms.Label();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.label2 = new System.Windows.Forms.Label();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.label3 = new System.Windows.Forms.Label();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtXuatXu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDanhMuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageHH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNhaSX.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTram.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbVATGB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbVATGV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaNhap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCheckVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbHienThi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTonKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDonViTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTT)).BeginInit();
            this.groupControlTT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNhaCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Appearance.ControlDropDown.Image = ((System.Drawing.Image)(resources.GetObject("layoutControl1.Appearance.ControlDropDown.Image")));
            this.layoutControl1.Appearance.ControlDropDown.Options.UseImage = true;
            this.layoutControl1.Controls.Add(this.label3);
            this.layoutControl1.Controls.Add(this.label2);
            this.layoutControl1.Controls.Add(this.label1);
            this.layoutControl1.Controls.Add(this.txtGhiChu);
            this.layoutControl1.Controls.Add(this.txtXuatXu);
            this.layoutControl1.Controls.Add(this.cbDanhMuc);
            this.layoutControl1.Controls.Add(this.imageHH);
            this.layoutControl1.Controls.Add(this.btnUpload);
            this.layoutControl1.Controls.Add(this.btnThemDVT);
            this.layoutControl1.Controls.Add(this.btnThemHH);
            this.layoutControl1.Controls.Add(this.btnLuuTT);
            this.layoutControl1.Controls.Add(this.cbNhaSX);
            this.layoutControl1.Controls.Add(this.cbThuocTinh);
            this.layoutControl1.Controls.Add(this.txtPhanTram);
            this.layoutControl1.Controls.Add(this.cbVATGB);
            this.layoutControl1.Controls.Add(this.txtGiaBan);
            this.layoutControl1.Controls.Add(this.cbVATGV);
            this.layoutControl1.Controls.Add(this.txtGiaNhap);
            this.layoutControl1.Controls.Add(this.cbCheckVAT);
            this.layoutControl1.Controls.Add(this.cbHienThi);
            this.layoutControl1.Controls.Add(this.txtTonKho);
            this.layoutControl1.Controls.Add(this.btnThemNSX);
            this.layoutControl1.Controls.Add(this.btnAddDM);
            this.layoutControl1.Controls.Add(this.cbDonViTinh);
            this.layoutControl1.Controls.Add(this.txtMaHH);
            this.layoutControl1.Controls.Add(this.txtTenHH);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(874, 56, 650, 400);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1215, 238);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(805, 123);
            this.txtGhiChu.MinimumSize = new System.Drawing.Size(0, 50);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(398, 61);
            this.txtGhiChu.TabIndex = 30;
            this.txtGhiChu.Text = "";
            // 
            // txtXuatXu
            // 
            this.txtXuatXu.Location = new System.Drawing.Point(805, 99);
            this.txtXuatXu.Name = "txtXuatXu";
            this.txtXuatXu.Size = new System.Drawing.Size(398, 20);
            this.txtXuatXu.StyleController = this.layoutControl1;
            this.txtXuatXu.TabIndex = 29;
            // 
            // cbDanhMuc
            // 
            this.cbDanhMuc.Location = new System.Drawing.Point(254, 41);
            this.cbDanhMuc.MinimumSize = new System.Drawing.Size(0, 25);
            this.cbDanhMuc.Name = "cbDanhMuc";
            this.cbDanhMuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDanhMuc.Properties.NullText = "Danh mục hàng hóa";
            this.cbDanhMuc.Properties.PopupView = this.gridLookUpEdit1View;
            this.cbDanhMuc.Size = new System.Drawing.Size(356, 25);
            this.cbDanhMuc.StyleController = this.layoutControl1;
            this.cbDanhMuc.TabIndex = 9;
            this.cbDanhMuc.TextChanged += new System.EventHandler(this.cbDanhMuc_TextChanged);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // imageHH
            // 
            this.imageHH.Location = new System.Drawing.Point(12, 12);
            this.imageHH.Name = "imageHH";
            this.imageHH.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.imageHH.Size = new System.Drawing.Size(140, 172);
            this.imageHH.StyleController = this.layoutControl1;
            this.imageHH.TabIndex = 28;
            // 
            // btnUpload
            // 
            this.btnUpload.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnUpload.ImageOptions.Image")));
            this.btnUpload.Location = new System.Drawing.Point(12, 188);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(140, 38);
            this.btnUpload.StyleController = this.layoutControl1;
            this.btnUpload.TabIndex = 27;
            this.btnUpload.Text = "Chọn ảnh";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnThemDVT
            // 
            this.btnThemDVT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnThemDVT.ImageOptions.Image")));
            this.btnThemDVT.Location = new System.Drawing.Point(1103, 12);
            this.btnThemDVT.Name = "btnThemDVT";
            this.btnThemDVT.Size = new System.Drawing.Size(100, 22);
            this.btnThemDVT.StyleController = this.layoutControl1;
            this.btnThemDVT.TabIndex = 26;
            this.btnThemDVT.Text = "Thêm";
            this.btnThemDVT.Click += new System.EventHandler(this.btnThemDVT_Click);
            // 
            // btnThemHH
            // 
            this.btnThemHH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnThemHH.ImageOptions.Image")));
            this.btnThemHH.Location = new System.Drawing.Point(849, 188);
            this.btnThemHH.Name = "btnThemHH";
            this.btnThemHH.Size = new System.Drawing.Size(354, 38);
            this.btnThemHH.StyleController = this.layoutControl1;
            this.btnThemHH.TabIndex = 25;
            this.btnThemHH.Text = "Thêm mới";
            this.btnThemHH.Click += new System.EventHandler(this.btnThemHH_Click);
            // 
            // btnLuuTT
            // 
            this.btnLuuTT.Enabled = false;
            this.btnLuuTT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuTT.ImageOptions.Image")));
            this.btnLuuTT.Location = new System.Drawing.Point(707, 188);
            this.btnLuuTT.Name = "btnLuuTT";
            this.btnLuuTT.Size = new System.Drawing.Size(138, 38);
            this.btnLuuTT.StyleController = this.layoutControl1;
            this.btnLuuTT.TabIndex = 22;
            this.btnLuuTT.Text = "Lưu thông tin";
            this.btnLuuTT.Click += new System.EventHandler(this.btnLuuTT_Click);
            // 
            // cbNhaSX
            // 
            this.cbNhaSX.Location = new System.Drawing.Point(805, 41);
            this.cbNhaSX.MinimumSize = new System.Drawing.Size(0, 25);
            this.cbNhaSX.Name = "cbNhaSX";
            this.cbNhaSX.Properties.AutoHeight = false;
            this.cbNhaSX.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbNhaSX.Size = new System.Drawing.Size(294, 25);
            this.cbNhaSX.StyleController = this.layoutControl1;
            this.cbNhaSX.TabIndex = 23;
            // 
            // cbThuocTinh
            // 
            this.cbThuocTinh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("cbThuocTinh.ImageOptions.Image")));
            this.cbThuocTinh.Location = new System.Drawing.Point(156, 186);
            this.cbThuocTinh.Name = "cbThuocTinh";
            this.cbThuocTinh.Size = new System.Drawing.Size(547, 38);
            this.cbThuocTinh.StyleController = this.layoutControl1;
            this.cbThuocTinh.TabIndex = 20;
            this.cbThuocTinh.Text = "Sản phẩm có thuộc tính";
            this.cbThuocTinh.CheckedChanged += new System.EventHandler(this.cbThuocTinh_CheckedChanged);
            // 
            // txtPhanTram
            // 
            this.txtPhanTram.EditValue = "0";
            this.txtPhanTram.Location = new System.Drawing.Point(254, 128);
            this.txtPhanTram.MinimumSize = new System.Drawing.Size(0, 25);
            this.txtPhanTram.Name = "txtPhanTram";
            this.txtPhanTram.Size = new System.Drawing.Size(356, 25);
            this.txtPhanTram.StyleController = this.layoutControl1;
            this.txtPhanTram.TabIndex = 19;
            this.txtPhanTram.TextChanged += new System.EventHandler(this.txtPhanTram_TextChanged);
            this.txtPhanTram.Validated += new System.EventHandler(this.txtPhanTram_Validated);
            // 
            // cbVATGB
            // 
            this.cbVATGB.Enabled = false;
            this.cbVATGB.Location = new System.Drawing.Point(614, 157);
            this.cbVATGB.Name = "cbVATGB";
            this.cbVATGB.Properties.Caption = "Bao gồm VAT";
            this.cbVATGB.Size = new System.Drawing.Size(89, 19);
            this.cbVATGB.StyleController = this.layoutControl1;
            this.cbVATGB.TabIndex = 18;
            // 
            // txtGiaBan
            // 
            this.txtGiaBan.EditValue = "0";
            this.txtGiaBan.Location = new System.Drawing.Point(254, 157);
            this.txtGiaBan.MinimumSize = new System.Drawing.Size(0, 25);
            this.txtGiaBan.Name = "txtGiaBan";
            this.txtGiaBan.Properties.NullText = "Giá bán";
            this.txtGiaBan.Size = new System.Drawing.Size(356, 25);
            this.txtGiaBan.StyleController = this.layoutControl1;
            this.txtGiaBan.TabIndex = 17;
            this.txtGiaBan.TextChanged += new System.EventHandler(this.txtGiaBan_TextChanged);
            this.txtGiaBan.Validated += new System.EventHandler(this.txtGiaBan_Validated);
            // 
            // cbVATGV
            // 
            this.cbVATGV.Location = new System.Drawing.Point(614, 99);
            this.cbVATGV.Name = "cbVATGV";
            this.cbVATGV.Properties.Caption = "Bao gồm VAT";
            this.cbVATGV.Size = new System.Drawing.Size(89, 19);
            this.cbVATGV.StyleController = this.layoutControl1;
            this.cbVATGV.TabIndex = 16;
            this.cbVATGV.CheckedChanged += new System.EventHandler(this.cbVATGV_CheckedChanged);
            // 
            // txtGiaNhap
            // 
            this.txtGiaNhap.EditValue = "0";
            this.txtGiaNhap.Location = new System.Drawing.Point(254, 99);
            this.txtGiaNhap.MinimumSize = new System.Drawing.Size(0, 25);
            this.txtGiaNhap.Name = "txtGiaNhap";
            this.txtGiaNhap.Size = new System.Drawing.Size(356, 25);
            this.txtGiaNhap.StyleController = this.layoutControl1;
            this.txtGiaNhap.TabIndex = 15;
            this.txtGiaNhap.TextChanged += new System.EventHandler(this.txtGiaNhap_TextChanged);
            this.txtGiaNhap.Validated += new System.EventHandler(this.txtGiaNhap_Validated);
            // 
            // cbCheckVAT
            // 
            this.cbCheckVAT.Location = new System.Drawing.Point(961, 70);
            this.cbCheckVAT.MinimumSize = new System.Drawing.Size(0, 25);
            this.cbCheckVAT.Name = "cbCheckVAT";
            this.cbCheckVAT.Properties.Caption = "Bán có VAT";
            this.cbCheckVAT.Size = new System.Drawing.Size(138, 19);
            this.cbCheckVAT.StyleController = this.layoutControl1;
            this.cbCheckVAT.TabIndex = 14;
            // 
            // cbHienThi
            // 
            this.cbHienThi.Location = new System.Drawing.Point(808, 70);
            this.cbHienThi.MinimumSize = new System.Drawing.Size(0, 25);
            this.cbHienThi.Name = "cbHienThi";
            this.cbHienThi.Properties.Caption = "Hiển thị";
            this.cbHienThi.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.cbHienThi.Size = new System.Drawing.Size(149, 25);
            this.cbHienThi.StyleController = this.layoutControl1;
            this.cbHienThi.TabIndex = 13;
            // 
            // txtTonKho
            // 
            this.txtTonKho.EditValue = "0";
            this.txtTonKho.Location = new System.Drawing.Point(254, 70);
            this.txtTonKho.MinimumSize = new System.Drawing.Size(0, 25);
            this.txtTonKho.Name = "txtTonKho";
            this.txtTonKho.Size = new System.Drawing.Size(356, 25);
            this.txtTonKho.StyleController = this.layoutControl1;
            this.txtTonKho.TabIndex = 12;
            this.txtTonKho.TextChanged += new System.EventHandler(this.txtTonKho_TextChanged);
            this.txtTonKho.Validated += new System.EventHandler(this.txtTonKho_Validated);
            // 
            // btnThemNSX
            // 
            this.btnThemNSX.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnThemNSX.ImageOptions.Image")));
            this.btnThemNSX.Location = new System.Drawing.Point(1103, 41);
            this.btnThemNSX.MinimumSize = new System.Drawing.Size(0, 25);
            this.btnThemNSX.Name = "btnThemNSX";
            this.btnThemNSX.Size = new System.Drawing.Size(100, 25);
            this.btnThemNSX.StyleController = this.layoutControl1;
            this.btnThemNSX.TabIndex = 11;
            this.btnThemNSX.Text = "Thêm mới";
            this.btnThemNSX.Click += new System.EventHandler(this.btnThemNSX_Click);
            // 
            // btnAddDM
            // 
            this.btnAddDM.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddDM.ImageOptions.Image")));
            this.btnAddDM.Location = new System.Drawing.Point(614, 41);
            this.btnAddDM.MinimumSize = new System.Drawing.Size(0, 25);
            this.btnAddDM.Name = "btnAddDM";
            this.btnAddDM.Size = new System.Drawing.Size(89, 25);
            this.btnAddDM.StyleController = this.layoutControl1;
            this.btnAddDM.TabIndex = 8;
            this.btnAddDM.Text = "Thêm mới";
            this.btnAddDM.Click += new System.EventHandler(this.btnAddDM_Click);
            // 
            // cbDonViTinh
            // 
            this.cbDonViTinh.EditValue = "Đơn vị tính";
            this.cbDonViTinh.Location = new System.Drawing.Point(893, 12);
            this.cbDonViTinh.MinimumSize = new System.Drawing.Size(0, 25);
            this.cbDonViTinh.Name = "cbDonViTinh";
            this.cbDonViTinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDonViTinh.Properties.NullText = "Đơn vị tính";
            this.cbDonViTinh.Properties.Sorted = true;
            this.cbDonViTinh.Size = new System.Drawing.Size(206, 25);
            this.cbDonViTinh.StyleController = this.layoutControl1;
            this.cbDonViTinh.TabIndex = 7;
            // 
            // txtMaHH
            // 
            this.txtMaHH.Location = new System.Drawing.Point(707, 12);
            this.txtMaHH.MinimumSize = new System.Drawing.Size(0, 25);
            this.txtMaHH.Name = "txtMaHH";
            this.txtMaHH.Properties.NullText = "Mã hàng hóa";
            this.txtMaHH.Size = new System.Drawing.Size(182, 25);
            this.txtMaHH.StyleController = this.layoutControl1;
            this.txtMaHH.TabIndex = 6;
            // 
            // txtTenHH
            // 
            this.txtTenHH.Location = new System.Drawing.Point(254, 12);
            this.txtTenHH.MinimumSize = new System.Drawing.Size(0, 25);
            this.txtTenHH.Name = "txtTenHH";
            this.txtTenHH.Properties.NullText = "Nhập tên hàng hóa";
            this.txtTenHH.Size = new System.Drawing.Size(449, 25);
            this.txtTenHH.StyleController = this.layoutControl1;
            this.txtTenHH.TabIndex = 5;
            this.txtTenHH.TextChanged += new System.EventHandler(this.txtTenHH_TextChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem17,
            this.layoutControlItem26,
            this.layoutControlItem7,
            this.layoutControlItem25,
            this.layoutControlItem24,
            this.layoutControlItem19,
            this.layoutControlItem1,
            this.layoutControlItem6,
            this.layoutControlItem27,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem13,
            this.layoutControlItem15,
            this.layoutControlItem28,
            this.layoutControlItem16,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1215, 238);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtTenHH;
            this.layoutControlItem2.Location = new System.Drawing.Point(144, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(551, 29);
            this.layoutControlItem2.Text = "Tên hàng hóa";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(95, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtMaHH;
            this.layoutControlItem3.Location = new System.Drawing.Point(695, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(186, 29);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.cbDonViTinh;
            this.layoutControlItem4.Location = new System.Drawing.Point(881, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(210, 29);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnAddDM;
            this.layoutControlItem5.Location = new System.Drawing.Point(602, 29);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(93, 29);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnThemNSX;
            this.layoutControlItem8.Location = new System.Drawing.Point(1091, 29);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(104, 58);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtTonKho;
            this.layoutControlItem9.Location = new System.Drawing.Point(144, 58);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(458, 29);
            this.layoutControlItem9.Text = "Tồn kho";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(95, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.cbHienThi;
            this.layoutControlItem10.Location = new System.Drawing.Point(796, 58);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(153, 29);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.cbCheckVAT;
            this.layoutControlItem11.Location = new System.Drawing.Point(949, 58);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(142, 29);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.cbThuocTinh;
            this.layoutControlItem17.Location = new System.Drawing.Point(144, 174);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(551, 44);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.cbNhaSX;
            this.layoutControlItem26.Location = new System.Drawing.Point(695, 29);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(396, 29);
            this.layoutControlItem26.Text = "Nhà sản xuất";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(95, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnThemHH;
            this.layoutControlItem7.Location = new System.Drawing.Point(837, 176);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(358, 42);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.btnLuuTT;
            this.layoutControlItem25.Location = new System.Drawing.Point(695, 176);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(142, 42);
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.btnThemDVT;
            this.layoutControlItem24.Location = new System.Drawing.Point(1091, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(104, 29);
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.btnUpload;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 176);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(144, 42);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.imageHH;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(144, 176);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.cbDanhMuc;
            this.layoutControlItem6.Location = new System.Drawing.Point(144, 29);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(458, 29);
            this.layoutControlItem6.Text = "Danh mục hàng hóa";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(95, 13);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.txtXuatXu;
            this.layoutControlItem27.Location = new System.Drawing.Point(695, 87);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(500, 24);
            this.layoutControlItem27.Text = "Xuất xứ";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(95, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txtGiaNhap;
            this.layoutControlItem12.Location = new System.Drawing.Point(144, 87);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(458, 29);
            this.layoutControlItem12.Text = "Giá nhập";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(95, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtGiaBan;
            this.layoutControlItem14.Location = new System.Drawing.Point(144, 145);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(458, 29);
            this.layoutControlItem14.Text = "Giá bán";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(95, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txtPhanTram;
            this.layoutControlItem16.Location = new System.Drawing.Point(144, 116);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(458, 29);
            this.layoutControlItem16.Text = "% tăng thêm";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(95, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.cbVATGV;
            this.layoutControlItem13.Location = new System.Drawing.Point(602, 87);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(93, 29);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.cbVATGB;
            this.layoutControlItem15.Location = new System.Drawing.Point(602, 145);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(93, 29);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.txtGhiChu;
            this.layoutControlItem28.Location = new System.Drawing.Point(695, 111);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(500, 65);
            this.layoutControlItem28.Text = "Ghi chú";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(95, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1016, 259);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.groupControlTT);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl4.Location = new System.Drawing.Point(0, 285);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup5;
            this.layoutControl4.Size = new System.Drawing.Size(1215, 227);
            this.layoutControl4.TabIndex = 4;
            this.layoutControl4.Text = "layoutControl4";
            this.layoutControl4.Visible = false;
            // 
            // groupControlTT
            // 
            this.groupControlTT.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("groupControlTT.CaptionImageOptions.Image")));
            this.groupControlTT.Controls.Add(this.simpleButton5);
            this.groupControlTT.Controls.Add(this.btnAddTT);
            this.groupControlTT.Location = new System.Drawing.Point(12, 12);
            this.groupControlTT.Name = "groupControlTT";
            this.groupControlTT.Size = new System.Drawing.Size(1191, 203);
            this.groupControlTT.TabIndex = 6;
            this.groupControlTT.Text = "THUỘC TÍNH HÀNG HÓA";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton5.ImageOptions.Image")));
            this.simpleButton5.Location = new System.Drawing.Point(230, 0);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(41, 36);
            this.simpleButton5.TabIndex = 8;
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // btnAddTT
            // 
            this.btnAddTT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddTT.ImageOptions.Image")));
            this.btnAddTT.Location = new System.Drawing.Point(177, 0);
            this.btnAddTT.Name = "btnAddTT";
            this.btnAddTT.Size = new System.Drawing.Size(44, 36);
            this.btnAddTT.TabIndex = 7;
            this.btnAddTT.Click += new System.EventHandler(this.btnAddTT_Click);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18});
            this.layoutControlGroup5.Name = "Root";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1215, 227);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.groupControlTT;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(1195, 207);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 10, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1215, 47);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.layoutControl5;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(1009, 47);
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControl5
            // 
            this.layoutControl5.Location = new System.Drawing.Point(2, 2);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.Root;
            this.layoutControl5.Size = new System.Drawing.Size(1005, 43);
            this.layoutControl5.TabIndex = 4;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1005, 43);
            this.Root.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(985, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.btnLuu;
            this.layoutControlItem22.Location = new System.Drawing.Point(1105, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(100, 47);
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextVisible = false;
            // 
            // btnLuu
            // 
            this.btnLuu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.ImageOptions.Image")));
            this.btnLuu.Location = new System.Drawing.Point(1107, 2);
            this.btnLuu.MaximumSize = new System.Drawing.Size(0, 30);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(96, 30);
            this.btnLuu.StyleController = this.layoutControl3;
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.btnHuy);
            this.layoutControl3.Controls.Add(this.btnLuu);
            this.layoutControl3.Controls.Add(this.layoutControl5);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl3.Location = new System.Drawing.Point(0, 238);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(1215, 47);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.TabStop = false;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // btnHuy
            // 
            this.btnHuy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnHuy.ImageOptions.Image")));
            this.btnHuy.Location = new System.Drawing.Point(1011, 2);
            this.btnHuy.MaximumSize = new System.Drawing.Size(0, 30);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(92, 30);
            this.btnHuy.StyleController = this.layoutControl3;
            this.btnHuy.TabIndex = 6;
            this.btnHuy.Text = "Hủy";
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.btnHuy;
            this.layoutControlItem23.Location = new System.Drawing.Point(1009, 0);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(96, 47);
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.grControl);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 512);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(1215, 215);
            this.layoutControl2.TabIndex = 5;
            this.layoutControl2.Text = "layoutControl2";
            this.layoutControl2.Visible = false;
            // 
            // grControl
            // 
            this.grControl.Location = new System.Drawing.Point(12, 12);
            this.grControl.MainView = this.grView;
            this.grControl.Name = "grControl";
            this.grControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnEdit,
            this.btnDelete});
            this.grControl.Size = new System.Drawing.Size(1191, 191);
            this.grControl.TabIndex = 4;
            this.grControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grView});
            // 
            // grView
            // 
            this.grView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.TenHH,
            this.MaHH,
            this.GiaVon,
            this.GiaBan,
            this.SoLuong,
            this.Edit,
            this.Delete});
            this.grView.GridControl = this.grControl;
            this.grView.Name = "grView";
            this.grView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grView_FocusedRowChanged);
            this.grView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grView_CellValueChanged);
            // 
            // TenHH
            // 
            this.TenHH.Caption = "Tên hàng hóa";
            this.TenHH.FieldName = "TenHang";
            this.TenHH.MinWidth = 200;
            this.TenHH.Name = "TenHH";
            this.TenHH.OptionsColumn.AllowEdit = false;
            this.TenHH.Visible = true;
            this.TenHH.VisibleIndex = 0;
            this.TenHH.Width = 200;
            // 
            // MaHH
            // 
            this.MaHH.Caption = "Mã hàng hóa";
            this.MaHH.FieldName = "MaHang";
            this.MaHH.MinWidth = 150;
            this.MaHH.Name = "MaHH";
            this.MaHH.OptionsColumn.AllowEdit = false;
            this.MaHH.Visible = true;
            this.MaHH.VisibleIndex = 1;
            this.MaHH.Width = 150;
            // 
            // GiaVon
            // 
            this.GiaVon.Caption = "Giá vốn";
            this.GiaVon.FieldName = "GiaMua";
            this.GiaVon.MinWidth = 100;
            this.GiaVon.Name = "GiaVon";
            this.GiaVon.OptionsColumn.AllowEdit = false;
            this.GiaVon.Visible = true;
            this.GiaVon.VisibleIndex = 2;
            this.GiaVon.Width = 100;
            // 
            // GiaBan
            // 
            this.GiaBan.Caption = "Giá bán";
            this.GiaBan.FieldName = "GiaBan";
            this.GiaBan.MinWidth = 150;
            this.GiaBan.Name = "GiaBan";
            this.GiaBan.Visible = true;
            this.GiaBan.VisibleIndex = 3;
            this.GiaBan.Width = 150;
            // 
            // SoLuong
            // 
            this.SoLuong.Caption = "Số lượng";
            this.SoLuong.FieldName = "SoLuong";
            this.SoLuong.MinWidth = 100;
            this.SoLuong.Name = "SoLuong";
            this.SoLuong.Visible = true;
            this.SoLuong.VisibleIndex = 4;
            this.SoLuong.Width = 100;
            // 
            // Edit
            // 
            this.Edit.Caption = "Cập nhật";
            this.Edit.ColumnEdit = this.btnEdit;
            this.Edit.MaxWidth = 50;
            this.Edit.MinWidth = 50;
            this.Edit.Name = "Edit";
            this.Edit.Visible = true;
            this.Edit.VisibleIndex = 5;
            this.Edit.Width = 50;
            // 
            // btnEdit
            // 
            this.btnEdit.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.btnEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnEdit_ButtonClick);
            // 
            // Delete
            // 
            this.Delete.Caption = "Ngừng kinh doanh";
            this.Delete.ColumnEdit = this.btnDelete;
            this.Delete.MaxWidth = 50;
            this.Delete.MinWidth = 50;
            this.Delete.Name = "Delete";
            this.Delete.Visible = true;
            this.Delete.VisibleIndex = 6;
            this.Delete.Width = 50;
            // 
            // btnDelete
            // 
            this.btnDelete.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.btnDelete.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnDelete.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnDelete_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1215, 215);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.grControl;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(1195, 195);
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // cbNhaCC
            // 
            this.cbNhaCC.Location = new System.Drawing.Point(711, 41);
            this.cbNhaCC.MinimumSize = new System.Drawing.Size(0, 25);
            this.cbNhaCC.Name = "cbNhaCC";
            this.cbNhaCC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbNhaCC.Properties.NullText = "Nhà cung cấp";
            this.cbNhaCC.Size = new System.Drawing.Size(380, 25);
            this.cbNhaCC.StyleController = this.layoutControl1;
            this.cbNhaCC.TabIndex = 10;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(614, 128);
            this.label1.MaximumSize = new System.Drawing.Size(0, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 25);
            this.label1.TabIndex = 31;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.label1;
            this.layoutControlItem29.Location = new System.Drawing.Point(602, 116);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(93, 29);
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextVisible = false;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(614, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 25);
            this.label2.TabIndex = 32;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.label2;
            this.layoutControlItem30.Location = new System.Drawing.Point(602, 58);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(93, 29);
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextVisible = false;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(707, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 25);
            this.label3.TabIndex = 33;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.label3;
            this.layoutControlItem31.Location = new System.Drawing.Point(695, 58);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(101, 29);
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextVisible = false;
            // 
            // frmEditHangHoa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1215, 727);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl4);
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl1);
            this.Name = "frmEditHangHoa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "THÊM MỚI HÀNG HÓA";
            this.Load += new System.EventHandler(this.frmEditHangHoa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtXuatXu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDanhMuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageHH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNhaSX.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhanTram.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbVATGB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaBan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbVATGV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaNhap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCheckVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbHienThi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTonKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDonViTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTT)).EndInit();
            this.groupControlTT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNhaCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cbDonViTinh;
        private DevExpress.XtraEditors.TextEdit txtMaHH;
        private DevExpress.XtraEditors.TextEdit txtTenHH;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btnAddDM;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton btnThemNSX;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit txtTonKho;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.CheckEdit cbCheckVAT;
        private DevExpress.XtraEditors.CheckEdit cbHienThi;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.CheckEdit cbVATGB;
        private DevExpress.XtraEditors.TextEdit txtGiaBan;
        private DevExpress.XtraEditors.CheckEdit cbVATGV;
        private DevExpress.XtraEditors.TextEdit txtGiaNhap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.CheckButton cbThuocTinh;
        private DevExpress.XtraEditors.TextEdit txtPhanTram;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
       
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.GroupControl groupControlTT;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton btnAddTT;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnHuy;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraGrid.GridControl grControl;
        private DevExpress.XtraGrid.Views.Grid.GridView grView;
        private DevExpress.XtraGrid.Columns.GridColumn TenHH;
        private DevExpress.XtraGrid.Columns.GridColumn MaHH;
        private DevExpress.XtraGrid.Columns.GridColumn GiaVon;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraGrid.Columns.GridColumn GiaBan;
        private DevExpress.XtraGrid.Columns.GridColumn SoLuong;
        private DevExpress.XtraGrid.Columns.GridColumn Edit;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnEdit;
        private DevExpress.XtraGrid.Columns.GridColumn Delete;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnLuuTT;
        private DevExpress.XtraEditors.ComboBoxEdit cbNhaCC;
        private DevExpress.XtraEditors.ComboBoxEdit cbNhaSX;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.SimpleButton btnThemHH;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.SimpleButton btnThemDVT;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraEditors.SimpleButton btnUpload;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.PictureEdit imageHH;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private DevExpress.XtraEditors.GridLookUpEdit cbDanhMuc;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.RichTextBox txtGhiChu;
        private DevExpress.XtraEditors.TextEdit txtXuatXu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
    }
}