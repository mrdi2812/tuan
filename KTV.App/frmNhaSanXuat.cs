﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KTV.Service;
using KTV.Model;
using System.Collections;

namespace KTV.App
{
    public partial class frmNhaSanXuat : DevExpress.XtraEditors.XtraForm
    {
        int index;
        NhaSXService service = new NhaSXService();
        public frmNhaSanXuat()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtTen.Text))
                {
                    NhaSanXuat model = new NhaSanXuat();
                    model.Name = txtTen.Text;
                    service.Add(model);                  
                    grNhaSX.DataBindings.Clear();
                    grNhaSX.DataSource = service.GetAll();
                    DialogResult dr;
                    dr = XtraMessageBox.Show("Thêm mới thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Không được để trống tên nhà sản xuất");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void frmNhaCungCap_Load(object sender, EventArgs e)
        {
            grNhaSX.DataSource = service.GetAll();
        }

    

     

        private void grView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            index = e.FocusedRowHandle;
        }

        private void btnUpdate_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            NhaSanXuat model = new NhaSanXuat();
            model.Id = int.Parse(grView.GetRowCellValue(index, grView.Columns[0]).ToString());
            model.Name = grView.GetRowCellValue(index, grView.Columns[1]).ToString();
            try
            {
                service.Update(model);
                service.Save();
                XtraMessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                grNhaSX.DataSource = service.GetAll();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnDelete_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DialogResult dr;
            dr = XtraMessageBox.Show("Bạn có muốn xóa các dòng đã chọn", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                service.Delete(int.Parse(grView.GetRowCellValue(index, grView.Columns[0]).ToString()));
                grNhaSX.DataSource = service.GetAll();
            }
        }

        private void txtTen_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                try
                {
                    if (!string.IsNullOrEmpty(txtTen.Text))
                    {
                        NhaSanXuat model = new NhaSanXuat();
                        model.Name = txtTen.Text;
                        service.Add(model);
                        grNhaSX.DataBindings.Clear();
                        grNhaSX.DataSource = service.GetAll();
                        DialogResult dr;
                        dr = XtraMessageBox.Show("Thêm mới thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Không được để trống tên nhà sản xuất");
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
    }
}