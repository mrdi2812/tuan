﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KTV.Service;
using KTV.Model;

namespace KTV.App
{
    public partial class frmThuocTinhHH : DevExpress.XtraEditors.XtraForm
    {
        ThuocTinhService service = new ThuocTinhService();
        int index;
        public frmThuocTinhHH()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtTen.Text))
                {
                    ThuocTinh model = new ThuocTinh();
                    model.Name = txtTen.Text;
                    service.Add(model);
                    LoadDL();
                    DialogResult dr;
                    dr = XtraMessageBox.Show("Thêm mới thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Không được để trống tên nhà sản xuất");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtTen_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                try
                {
                    if (!string.IsNullOrEmpty(txtTen.Text))
                    {
                        ThuocTinh model = new ThuocTinh();
                        model.Name = txtTen.Text;
                        service.Add(model);
                        LoadDL();
                        DialogResult dr;
                        dr = XtraMessageBox.Show("Thêm mới thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Không được để trống tên nhà sản xuất");
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
        public void LoadDL()
        {
            var model = service.GetAll();
            grControl.DataBindings.Clear();
            grControl.DataSource = model;

        }
        private void frmThuocTinhHH_Load(object sender, EventArgs e)
        {
            LoadDL();
        }

        private void btnUpdate_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            ThuocTinh model = new ThuocTinh();
            model.Id = int.Parse(grView.GetRowCellValue(index, grView.Columns[0]).ToString());
            model.Name = grView.GetRowCellValue(index, grView.Columns[1]).ToString();
            try
            {
                service.Update(model);
                service.Save();
                XtraMessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                grControl.DataSource = service.GetAll();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnDelete_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DialogResult dr;
            dr = XtraMessageBox.Show("Bạn có muốn xóa dòng đã chọn", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                service.Delete(int.Parse(grView.GetRowCellValue(index, grView.Columns[0]).ToString()));
                grControl.DataSource = service.GetAll();
            }
        }

        private void grView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            index = e.FocusedRowHandle;
        }
    }
}