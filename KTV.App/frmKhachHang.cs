﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KTV.Service;

namespace KTV.App
{
    public partial class frmKhachHang : DevExpress.XtraEditors.XtraForm
    {
        KhachHangService khachHangService = new KhachHangService();
        public frmKhachHang()
        {
            InitializeComponent();
        }
        public void LoadData()
        {
            grControl.DataBindings.Clear();
            grControl.DataSource = khachHangService.GetAll().ToList();
        }
        private void btnCopy_Click(object sender, EventArgs e)
        {

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

        }

        private void frmKhachHang_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmEditKhachHang kh = new frmEditKhachHang();
            kh.FormClosed += Kh_FormClosed;
            kh.ShowDialog();
        }

        private void Kh_FormClosed(object sender, FormClosedEventArgs e)
        {
            LoadData();
        }
    }
}