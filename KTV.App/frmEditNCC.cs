﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KTV.Service;
using KTV.Model;
using KTV.Data;

namespace KTV.App
{
    public partial class frmEditNCC : DevExpress.XtraEditors.XtraForm
    {
        NCCService nCCService = new NCCService();
        public frmEditNCC()
        {
            InitializeComponent();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTenNCC.Text != "")
                {
                    NhaCungCap model = new NhaCungCap();
                    model.DiaChi = txtDiachi.Text;
                    model.Email = txtEmail.Text;
                    model.GhiChu = txtGhichu.Text;
                    using (var ctx = new KTVDbContext())
                    {
                        var ListMa = ctx.Database.SqlQuery<GetMa>("exec CreateMa 'NhaCungCaps','Ma'").FirstOrDefault();
                        if (ListMa == null)
                        {
                            MessageBox.Show("Sinh mã lỗi, thử lại sau.");
                        }
                        model.Ma = ListMa.Ma;
                    }
                    model.MaSoThue = txtMaSoThue.Text;
                    model.Name = txtTenNCC.Text;
                    model.NguoiLienHe = txtNguoiLienHe.Text;
                    model.Phone = txtSDT.Text;
                    model.Status = true;
                    nCCService.Add(model);
                    MessageBox.Show("Thêm mới thành công");
                }
                else
                {
                    MessageBox.Show("Không được để trống tên nhà cung cấp");
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Không thành công");
            }
        }
    }
}