﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace KTV.App
{
    public partial class frmMain : DevExpress.XtraEditors.XtraForm
    {
        public frmMain()
        {
            InitializeComponent();
        }
        private Form KiemTraTonTai(Type type)
        {
            foreach (Form f in this.MdiChildren)
            {
                if (f.GetType() == type)
                {
                    return f;
                }
            }
            return null;
        }

        private void banhang_Click(object sender, EventArgs e)
        {
            Form frm = this.KiemTraTonTai(typeof(frmBanHang));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmBanHang child = new frmBanHang();
                child.MdiParent = this;
                child.Show();

            }

        }
        private void hanghoa_Click(object sender, EventArgs e)
        {

        }

        private void navBarItem6_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form frm = this.KiemTraTonTai(typeof(frmHangHoa));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmHangHoa child = new frmHangHoa();
                child.MdiParent = this;
                child.Show();

            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void btnPhieuThu_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Form frm = this.KiemTraTonTai(typeof(frmPhieuThu));
            if (frm != null)
            {
                frm.Activate();
            }
            else
            {
                frmPhieuThu child = new frmPhieuThu();
                child.MdiParent = this;
                child.Show();

            }
        }
    }
}