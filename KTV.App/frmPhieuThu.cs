﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KTV.Service;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;

namespace KTV.App
{
    public partial class frmPhieuThu : DevExpress.XtraEditors.XtraForm
    {
        int index;
        PhieuThuService phieuThuService = new PhieuThuService();
        KhachHangService khachHangService = new KhachHangService();

        public frmPhieuThu()
        {
            InitializeComponent();
        }

        private void grView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            index = e.FocusedRowHandle;           
        }

        private void btnSeach_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

        }

        private void frmPhieuThu_Load(object sender, EventArgs e)
        {
            GetData();
            flyoutPanel1.Hide();
        }

        public void GetData()
        {
            grControl.DataBindings.Clear();
            grControl.DataSource = phieuThuService.GetAll();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

        }

        private void btnAddPT_Click(object sender, EventArgs e)
        {
            frmEditPhieuThu con = new frmEditPhieuThu();
            con.FormClosed += Con_FormClosed;
            con.ShowDialog();
        }

        private void Con_FormClosed(object sender, FormClosedEventArgs e)
        {
            GetData();
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            var model = phieuThuService.GetByID(grView.GetRowCellValue(index, "MaPhieuThu").ToString());
            var tt = khachHangService.GetById(model.KhachHangID.Value);
            lblmaPhieuThu.Text = grView.GetRowCellValue(index, "MaPhieuThu").ToString();
            lblGhiChu.Text = model.GhiChu;
            lblTenNguoiNop.Text = tt.Name;
            Cursor = new Cursor(Cursor.Current.Handle);
            Cursor.Position = new Point(Cursor.Position.X + 40, Cursor.Position.Y);
            flyoutPanel1.ShowBeakForm(Cursor.Position);
            Cursor.Position = new Point(Cursor.Position.X - 40, Cursor.Position.Y);
        }

        private void lblTenNguoiNop_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmTTKH tt = new frmTTKH();           
            tt.ID = int.Parse(grView.GetRowCellValue(index, "KhachHangID").ToString());
            tt.Show();
        }
    }
}