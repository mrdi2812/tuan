﻿using DevExpress.XtraEditors;
using KTV.Common.Models;
using KTV.Model;
using KTV.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KTV.App
{
    public partial class frmTTKH : Form
    {
        int index;
        public int ID { set; get;}
        KhachHangService khService = new KhachHangService();
        BanHangService banHangService = new BanHangService();
        public frmTTKH()
        {
            InitializeComponent();
        }

        private void grView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {

        }

        private void frmTTKH_Load(object sender, EventArgs e)
        {
            var model = khService.GetById(ID);
            lblTenKhachhang.Text = model.Name + "( " + model.Ma +" )";
            txtDiachi.Text = model.DiaChi;
            txtEmail.Text = model.Email;
            txtGhiChu.Text = model.GhiChu;
            txtMasothue.Text = model.MaSoThue;
            txtNguoilienhe.Text = model.NguoiLienHe;
            txtSodienthoai.Text = model.Phone;
            if(model.NgaySinh.Value!=null)
                dateNgaySinh.Text = model.NgaySinh.Value.ToShortDateString();
            if (model.GioiTinh == "Nam")
                radioGroup1.SelectedIndex = 0;
            if(model.GioiTinh == "Nữ")
                radioGroup1.SelectedIndex = 1;
            if (model.GioiTinh == "Khác")
                radioGroup1.SelectedIndex = 2;
            txtDonhang.Text = banHangService.TongDonByKH(ID).ToString();
            txtTienHang.Text = banHangService.TongTienByKH(ID).ToString();
            txtTongNo.Text = banHangService.TongNoByKH(ID).ToString();
            lblTongNoMH.Text = banHangService.TongNoByKH(ID).ToString();
            lblTongTienMH.Text = banHangService.TongTienByKH(ID).ToString();                    
            LoadData();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

        }
        public void LoadData()
        {
            grMuaHang.DataBindings.Clear();
            grMuaHang.DataSource = banHangService.GetByMaKH(ID).ToList();
            grHangHoa.DataBindings.Clear();
            var model = banHangService.GetAllSPByKH(ID).ToList();
            grHangHoa.DataSource = model;
            lblTongSL.Text = model.Sum(x => x.SoLuong).ToString();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var model = khService.GetById(ID);
                model.MaSoThue = txtMasothue.Text;
                model.NgaySinh = DateTime.Parse(dateNgaySinh.Text);
                model.NguoiLienHe = txtNguoilienhe.Text;
                model.Phone = txtSodienthoai.Text;
                model.DiaChi = txtDiachi.Text;
                model.Email = txtEmail.Text;
                model.GhiChu = txtGhiChu.Text;
                model.GioiTinh = radioGroup1.Text;
                khService.Update(model);
                MessageBox.Show("Cập nhật thành công");
            }
            catch (Exception)
            {
                MessageBox.Show("Không thành công");
            }
        }

        private void btnPrintThuChi_Click(object sender, EventArgs e)
        {

        }
    }
}
