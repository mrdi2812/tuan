﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KTV.Service;
using KTV.Model;
using DevExpress.XtraPrinting;
using KTV.App.Template;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraReports.UI;

namespace KTV.App
{
    public partial class frmInMaVach : DevExpress.XtraEditors.XtraForm
    {
        HangHoaService hangHoaService = new HangHoaService();
        ThongTinService thongTinService = new ThongTinService();
        List<HangHoa> list = new List<HangHoa>();
        public frmInMaVach()
        {
            InitializeComponent();
        }

        private void frmInMaVach_Load(object sender, EventArgs e)
        {
            LoadDL();
            radioGroupKhoIn.SelectedIndex = 1;
            radioGroupThuocTinh.SelectedIndex = 3;
        }
        public void LoadDL()
        {
            var model = hangHoaService.GetAllHangHoaMH();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[2] { new DataColumn("ID"), new DataColumn("Ten") });
            foreach (var item in model)
            {
                dt.Rows.Add(item.ID, item.TenHang);
            }
            cbCheck.Properties.DataSource = dt;
            cbCheck.Properties.ValueMember = "ID";
            cbCheck.Properties.DisplayMember = "Ten";
        }

        private void cbCheck_EditValueChanged(object sender, EventArgs e)
        {
            list.Clear();
            grControl.DataSource = null;
            for (int i = 0; i < cbCheck.Properties.Items.Count; i++)
            {
                if (cbCheck.Properties.Items[i].CheckState == System.Windows.Forms.CheckState.Checked)
                {
                    var item = hangHoaService.GetById(int.Parse(cbCheck.Properties.Items[i].Value.ToString()));
                    item.SoLuong = 1;
                    if (!list.Exists(x => x.ID == item.ID))
                    {
                        list.Add(item);
                    }                  
                }
            }
            grControl.DataBindings.Clear();
            grControl.DataSource = list;
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            var tt = thongTinService.GetDefault();
            if (radioGroupKhoIn.SelectedIndex!=-1&&radioGroupThuocTinh.SelectedIndex!=-1)
            {
                List<HangHoa> list = new List<HangHoa>();
                for (int i = 0; i < grView.RowCount; i++)
                {
                    int index = grView.GetVisibleRowHandle(i);
                    int count = int.Parse(grView.GetRowCellValue(index, "SoLuong").ToString());
                    var item = hangHoaService.GetByMaHang(grView.GetRowCellValue(index, "MaHang").ToString());
                    for (int j = 1; j <= count; j++)
                    {
                        list.Add(item);
                    }
                }
                int trai = int.Parse(txtLetrai.Text);
                int phai = int.Parse(txtLephai.Text);
                int tren = int.Parse(txtLetren.Text);
                int duoi = int.Parse(txtLeduoi.Text);
                //MessageBox.Show(radioGroupKhoIn.Properties.Items[radioGroupKhoIn.SelectedIndex].Description);
                switch (radioGroupKhoIn.SelectedIndex)
                {
                    case 0:
                        switch (radioGroupThuocTinh.SelectedIndex)
                        {
                            case 0:
                                break;
                            case 1:
                                break;
                            case 2:
                                break;
                            default:
                                A540 report = new A540();
                                report.DataSource = list;
                                report.Margins = new System.Drawing.Printing.Margins(trai, phai, tren, duoi);                               
                                report.PaperKind = System.Drawing.Printing.PaperKind.Custom;
                                report.PageWidth = 827;
                                report.PageHeight = 583;
                                //report.Padding = new PaddingInfo(50, 50, 50, 50);
                                using (ReportPrintTool printTool = new ReportPrintTool(report))
                                {
                                    printTool.PrintDialog();
                                }
                                break;
                        }
                        break;
                    case 2:
                        switch (radioGroupThuocTinh.SelectedIndex)
                        {
                            case 0:
                                break;
                            case 1:
                                break;
                            case 2:
                                break;
                            default:
                                TemKinhMat report = new TemKinhMat(tt.Name,tt.Address,tt.GhiChu);
                                report.DataSource = list;
                                report.Margins = new System.Drawing.Printing.Margins(trai, phai, tren, duoi);
                                report.PaperKind = System.Drawing.Printing.PaperKind.Custom;
                                report.PageHeight = 260;
                                report.PageWidth = 125;
                                report.PrinterName = "TemNhan";
                                using (ReportPrintTool printTool = new ReportPrintTool(report))
                                { 
                                    printTool.PrintDialog();
                                }
                                break;
                        }
                        break;                  
                    default:
                        switch (radioGroupThuocTinh.SelectedIndex)
                        {
                            case 0:
                                break;
                            case 1:
                                break;
                            case 2:
                                break;
                            default:
                                A4No65 report = new A4No65();
                                report.DataSource = list;
                                report.Margins = new System.Drawing.Printing.Margins(trai,phai,tren,duoi);
                                report.PaperKind = System.Drawing.Printing.PaperKind.A4;
                                using (ReportPrintTool printTool = new ReportPrintTool(report))
                                {
                                    printTool.PrintDialog();
                                }
                                break;
                        }
                        break;
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn 1 loại khổ in và 1 thuộc tính");
            }
        }
    }
}