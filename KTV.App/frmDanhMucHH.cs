﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KTV.Service;
using KTV.Model;

namespace KTV.App
{
    public partial class frmDanhMucHH : DevExpress.XtraEditors.XtraForm
    {
        int index;
        NhomHHService nhomHHService = new NhomHHService();
        public frmDanhMucHH()
        {
            InitializeComponent();
        }         
        public void LoadData()
        {
            var listmh = nhomHHService.GetAll();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[2] { new DataColumn("ID"), new DataColumn("Ten") });
            foreach (var item in listmh)
            {
                dt.Rows.Add(item.Id, item.Name);
            }
            cbDanhMuc.Properties.DataSource = dt;
            cbDanhMuc.Properties.ValueMember = "ID";
            cbDanhMuc.Properties.DisplayMember = "Ten";
            grDanhMuc.DataSource = dt;
            grDanhMuc.ValueMember = "ID";
            grDanhMuc.DisplayMember = "Ten";
            grControl.DataBindings.Clear();
            grControl.DataSource = nhomHHService.GetAll();

        }
        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtTenHH.Text))
                {
                    NhomHangHoa model = new NhomHangHoa();
                    model.Name = txtTenHH.Text; 
                    if(cbDanhMuc.Text!= "[EditValue is null]")
                    {
                        model.NhomHangId = int.Parse(cbDanhMuc.EditValue.ToString());
                    }
                    model.Stt = int.Parse(txtSoTT.Text);
                    nhomHHService.Add(model);
                    grControl.DataBindings.Clear();
                    grControl.DataSource = nhomHHService.GetAll();
                    grControl.ForceInitialize();
                    DialogResult dr;
                    dr = XtraMessageBox.Show("Thêm mới thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        this.Close();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void frmDanhMucHH_Load(object sender, EventArgs e)
        {
            LoadData();            
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dr;
                dr = XtraMessageBox.Show("Bạn có muốn cập nhật danh mục này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    var model = nhomHHService.GetById(int.Parse(grView.GetRowCellValue(index, "Id").ToString()));
                    model.Name = grView.GetRowCellValue(index, "Name").ToString();
                    if(grView.GetRowCellValue(index, "NhomHangId").ToString() != "")
                    {
                        model.NhomHangId = int.Parse(grView.GetRowCellValue(index, "NhomHangId").ToString());
                    }    
                    model.Stt = int.Parse(grView.GetRowCellValue(index, "Stt").ToString());     
                    MessageBox.Show("Thành công");
                    LoadData();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi xóa không thành công");
            }
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dr;
                dr = XtraMessageBox.Show("Bạn có muốn xóa danh mục này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    nhomHHService.Delete(int.Parse(grView.GetRowCellValue(index, "Id").ToString()));
                    MessageBox.Show("Thành công");
                    LoadData();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi xóa không thành công");
            }
        }

        private void grView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            index = e.FocusedRowHandle;
        }
    }
}