﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KTV.Service;
using KTV.Model;
using KTV.Data;

namespace KTV.App
{
    public partial class frmEditKhachHang : DevExpress.XtraEditors.XtraForm
    {
        KhachHangService khService = new KhachHangService();
        public frmEditKhachHang()
        {
            InitializeComponent();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTenKH.Text != "" && txtSodienthoai.Text != "")
                {
                    KhachHang kh = new KhachHang();
                    kh.DiaChi = txtDiaChi.Text;
                    kh.Email = txtEmail.Text;
                    kh.GhiChu = txtGhichu.Text;
                    kh.GioiTinh = radioGroupGT.Text;
                    using (var ctx = new KTVDbContext())
                    {
                        var ListMa = ctx.Database.SqlQuery<GetMa>("exec CreateMa 'KhachHangs','Ma'").FirstOrDefault();
                        if (ListMa == null)
                        {
                            MessageBox.Show("Sinh mã lỗi, thử lại sau.");
                        }
                        kh.Ma = ListMa.Ma;
                    }
                    kh.MaSoThue = txtMaSoThue.Text;
                    kh.Name = txtTenKH.Text;
                    if (dateNgaySinh.Text != "")
                    {
                        kh.NgaySinh = DateTime.Parse(dateNgaySinh.Text);
                    }                    
                    kh.NguoiLienHe = txtNguoiLienHe.Text;
                    kh.Phone = txtSodienthoai.Text;
                    kh.Status = true;
                    khService.Add(kh);
                    MessageBox.Show("Thêm mới thành công.");
                }
                else
                {
                    MessageBox.Show("Không được để trống tên và số điện thoại.");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Không thành công");
            }
        }
    }
}