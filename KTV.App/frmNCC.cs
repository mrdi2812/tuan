﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KTV.Service;

namespace KTV.App
{
    public partial class frmNCC : DevExpress.XtraEditors.XtraForm
    {
        NCCService nCCService = new NCCService();
        public frmNCC()
        {
            InitializeComponent();
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmEditNCC child = new frmEditNCC();
            child.FormClosed += Child_FormClosed;
            child.ShowDialog();
        }

        private void Child_FormClosed(object sender, FormClosedEventArgs e)
        {
            LoadData();
        }
        public void LoadData()
        {
            grControl.DataBindings.Clear();
            grControl.DataSource = nCCService.GetAll();
        }
        private void frmNCC_Load(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}