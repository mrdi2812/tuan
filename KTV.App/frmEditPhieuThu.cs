﻿using DevExpress.XtraEditors;
using KTV.Data;
using KTV.Model;
using KTV.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KTV.App
{
    public partial class frmEditPhieuThu : Form
    {
        KhachHangService khachHangService = new KhachHangService();
        PhieuThuService phieuThuService = new PhieuThuService();
        public frmEditPhieuThu()
        {
            InitializeComponent();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void GetData()
        {
            var listmh = khachHangService.GetAll();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[3] { new DataColumn("ID"), new DataColumn("Ten"), new DataColumn("SDT") });
            foreach (var item in listmh)
            {
                dt.Rows.Add(item.Id, item.Name,item.Phone);
            }
            listNguoiNopTien.Properties.DataSource = dt;
            listNguoiNopTien.Properties.ValueMember = "ID";
            listNguoiNopTien.Properties.DisplayMember = "Ten";
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (listNguoiNopTien.Text != "" && txtTongTien.Text != "" && radioPTTT.Text != ""&&cbLoaiThu.Text!="")
                {
                    PhieuThu pt = new PhieuThu();
                    pt.GhiChu = txtGhiChu.Text;
                    pt.PTThanhToan = radioPTTT.Text;
                    pt.KhachHangID = int.Parse(listNguoiNopTien.EditValue.ToString());
                    using (var ctx = new KTVDbContext())
                    {
                        var ListMa = ctx.Database.SqlQuery<GetMa>("exec CreateMa 'PhieuThus','MaPhieuThu'").FirstOrDefault();
                        if (ListMa == null)
                        {
                            MessageBox.Show("Sinh mã lỗi, thử lại sau.");
                        }
                        pt.MaPhieuThu = ListMa.Ma;
                    }
                    pt.NgayThu = DateTime.Parse(dateNgayChi.Text);
                    pt.HangMucThu = cbLoaiThu.Text;
                    pt.TongTien = double.Parse(txtTongTien.Text);
                    phieuThuService.Add(pt);
                    DialogResult dr;
                    dr = XtraMessageBox.Show("Thêm mới thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Question);
                    if (dr == DialogResult.OK)
                    {
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Không được để trống");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Không thành công");
            }
        }

        private void frmEditPhieuThu_Load(object sender, EventArgs e)
        {
            GetData();
            dateNgayChi.Text = DateTime.Now.ToShortDateString();
        }

        private void txtTongTien_Validated(object sender, EventArgs e)
        {
            if (txtTongTien.Text.All(Char.IsNumber) == false)
            {
                MessageBox.Show("Vui lòng nhập kiểu số");
                txtTongTien.Text = "";
            }                
        }

        private void txtTongTien_Validating(object sender, CancelEventArgs e)
        {
            if (txtTongTien.Text.All(Char.IsNumber) == true && int.Parse(txtTongTien.Text) < 1000)
            {
                MessageBox.Show("Số tiền phải lớn hơn 1000");
            }
            if (txtTongTien.Text == "" || string.IsNullOrEmpty(txtTongTien.Text))
            {
                MessageBox.Show("Không được để trống");
            }
        }
    }
}
