﻿using DevExpress.XtraEditors;
using KTV.Model;
using KTV.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KTV.App
{
    public partial class frmDonViTinh : Form
    {
        DonViTinhService donViTinhService = new DonViTinhService();
        int index;
        public frmDonViTinh()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtTen.Text))
                {
                    DonViTinh model = new DonViTinh();
                    model.Name = txtTen.Text;
                    donViTinhService.Add(model);
                    LoadDL();
                    DialogResult dr;
                    dr = XtraMessageBox.Show("Thêm mới thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Không được để trống tên đơn vị tính");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void frmDonViTinh_Load(object sender, EventArgs e)
        {
            LoadDL();
        }
        public void LoadDL()
        {
            grControl.DataBindings.Clear();
            grControl.DataSource = donViTinhService.GetAll();
        }

        private void txtTen_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                try
                {
                    if (!string.IsNullOrEmpty(txtTen.Text))
                    {
                        DonViTinh model = new DonViTinh();
                        model.Name = txtTen.Text;
                        donViTinhService.Add(model);
                        LoadDL();
                        DialogResult dr;
                        dr = XtraMessageBox.Show("Thêm mới thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Không được để trống tên đơn vị tính");
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        private void btnEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DonViTinh model = new DonViTinh();
            model.Id = int.Parse(grView.GetRowCellValue(index, grView.Columns[0]).ToString());
            model.Name = grView.GetRowCellValue(index, grView.Columns[1]).ToString();
            try
            {
                donViTinhService.Update(model);
                XtraMessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                grControl.DataSource = donViTinhService.GetAll();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnXoa_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DialogResult dr;
            dr = XtraMessageBox.Show("Bạn có muốn xóa dòng đã chọn", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                donViTinhService.Delete(int.Parse(grView.GetRowCellValue(index, grView.Columns[0]).ToString()));
                grControl.DataSource = donViTinhService.GetAll();
            }
        }

        private void grView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            index = e.FocusedRowHandle;
        }
    }
}
