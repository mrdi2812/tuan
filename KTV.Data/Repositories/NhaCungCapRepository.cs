﻿using KTV.Data.Infrastructure;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Data.Repositories
{
    public interface INhaCungCapRepository : IRepository<NhaCungCap>
    {

    }
    public class NhaCungCapRepository : RepositoryBase<NhaCungCap>, INhaCungCapRepository
    {
        public NhaCungCapRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
