﻿using KTV.Data.Infrastructure;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Data.Repositories
{
    public interface INhomKhachHangRepository : IRepository<NhomKhachHang>
    {

    }
    public class NhomKhachHangRepository : RepositoryBase<NhomKhachHang>, INhomKhachHangRepository
    {
        public NhomKhachHangRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
