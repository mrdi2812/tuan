﻿using KTV.Data.Infrastructure;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Data.Repositories
{
    public interface INhaSanXuatRepository : IRepository<NhaSanXuat>
    {

    }
    public class NhaSanXuatRepository : RepositoryBase<NhaSanXuat>, INhaSanXuatRepository
    {
        public NhaSanXuatRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
