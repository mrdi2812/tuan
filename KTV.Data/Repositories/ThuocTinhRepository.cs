﻿using KTV.Data.Infrastructure;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Data.Repositories
{
    public interface IThuocTinhRepository : IRepository<ThuocTinh>
    {

    }
    public class ThuocTinhRepository : RepositoryBase<ThuocTinh>, IThuocTinhRepository
    {
        public ThuocTinhRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
