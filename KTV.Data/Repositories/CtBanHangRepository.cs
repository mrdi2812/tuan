﻿using KTV.Data.Infrastructure;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Data.Repositories
{
    public interface ICtBanHangRepository : IRepository<CtBanHang>
    {

    }
    public class CtBanHangRepository : RepositoryBase<CtBanHang>, ICtBanHangRepository
    {
        public CtBanHangRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}
