﻿using KTV.Data.Infrastructure;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Data.Repositories
{
    public interface IKhachHangGroupRepository : IRepository<KhachHangGroup>
    {

    }
    public class KhachHangGroupRepository : RepositoryBase<KhachHangGroup>, IKhachHangGroupRepository
    {
        public KhachHangGroupRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
