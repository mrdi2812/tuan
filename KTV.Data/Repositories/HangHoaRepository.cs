﻿using KTV.Data.Infrastructure;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Data.Repositories
{
    public interface IHangHoaRepository : IRepository<HangHoa>
    {
       
    }
    public class HangHoaRepository : RepositoryBase<HangHoa>,IHangHoaRepository
    {
        public HangHoaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    
    }
}
