﻿using KTV.Data.Infrastructure;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Data.Repositories
{
    public interface ICtTienRepository : IRepository<CtTien>
    {

    }
    public class CtTienRepository : RepositoryBase<CtTien>,ICtTienRepository
    {
        public CtTienRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
