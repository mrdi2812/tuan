﻿using KTV.Data.Infrastructure;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Data.Repositories
{
    public interface ICtTonRepository : IRepository<CtTon>
    {

    }
    public class CtTonRepository : RepositoryBase<CtTon>, ICtTonRepository
    {
        public CtTonRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
