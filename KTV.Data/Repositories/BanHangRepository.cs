﻿using KTV.Data.Infrastructure;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Data.Repositories
{
    public interface IBanHangRepository : IRepository<BanHang>
    {

    }
    public class BanHangRepository : RepositoryBase<BanHang>, IBanHangRepository
    {
        public BanHangRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
