﻿using KTV.Data.Infrastructure;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Data.Repositories
{
    public interface IThuocTinhGroupRepository : IRepository<ThuocTinhGroup>
    {

    }
    public class ThuocTinhGroupRepository : RepositoryBase<ThuocTinhGroup>, IThuocTinhGroupRepository
    {
        public ThuocTinhGroupRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
