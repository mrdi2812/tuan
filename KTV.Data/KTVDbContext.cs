﻿using KTV.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace KTV.Data
{
    public class KTVDbContext : DbContext
    {
        public KTVDbContext() : base("KTVConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
        public DbSet<BanHang> BanHangs { set; get; }
        public DbSet<CtBanHang> CtBanHangs { set; get; }
        public DbSet<CtTien> CtTiens { set; get; }
        public DbSet<CtTon> CtTons { set; get; }
        public DbSet<HangHoa> HangHoas { set; get; }
        public DbSet<KhachHang> KhachHangs { set; get; }
        public DbSet<KhachHangGroup> KhachHangGroups { set; get; }
        public DbSet<NhaCungCap> NhaCungCaps { set; get; }
        public DbSet<NhaSanXuat> NhaSanXuats { set; get; }
        public DbSet<NhomKhachHang> NhomKhachHangs { set; get; }
        public DbSet<ThuocTinh> ThuocTinhs { set; get; }
        public DbSet<ThuocTinhGroup> ThuocTinhGroups { set; get; }
        public DbSet<DonViTinh> DonViTinhs { set; get; }
        public DbSet<NhomHangHoa> NhomHangHoas { set; get; }
        public DbSet<SoThuTu> SoThuTus { set; get; }
        public DbSet<ThongTin> ThongTins { set; get; }
        public DbSet<CauHinhHeThong> CauHinhHeThongs { set; get; }
        public DbSet<PhieuChi> PhieuChis { set; get; }
        public DbSet<PhieuThu> PhieuThus { set; get; }
        public static KTVDbContext Create()
        {
            return new KTVDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder builder)
        {  
            
        }
    }
}
