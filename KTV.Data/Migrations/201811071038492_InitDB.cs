namespace KTV.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BanHangs",
                c => new
                    {
                        MaHD = c.String(nullable: false, maxLength: 128),
                        LoaiHD = c.String(),
                        NgayBan = c.DateTime(nullable: false),
                        NVBH = c.Int(),
                        NVTN = c.Int(),
                        NVKT = c.Int(),
                        NVKho = c.Int(),
                        KhachHangId = c.Int(),
                        Status = c.Int(nullable: false),
                        TongTien = c.Double(nullable: false),
                        TongSL = c.Double(nullable: false),
                        SoGiaoDich = c.String(),
                        KhoId = c.Int(),
                        GhiChu = c.String(),
                        KhuyenMai = c.Double(nullable: false),
                        KhachDua_Tong = c.Double(nullable: false),
                        KhachDua_TienMat = c.Double(nullable: false),
                        KhachDua_The = c.Double(nullable: false),
                        KhachDua_ChuyenKhoan = c.Double(nullable: false),
                        Congno = c.Double(nullable: false),
                        Tralai = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.MaHD);
            
            CreateTable(
                "dbo.CauHinhHeThongs",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 250),
                        Descript = c.String(nullable: false),
                        Value = c.String(nullable: false),
                        Default = c.String(nullable: false),
                        User_Create = c.String(),
                        User_Edit = c.String(),
                        Datetime_Create = c.DateTime(),
                        Datetime_Edit = c.DateTime(),
                    })
                .PrimaryKey(t => t.Name);
            
            CreateTable(
                "dbo.CtBanHangs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaHD = c.String(),
                        MaHang = c.String(),
                        SoLuong = c.Double(nullable: false),
                        GiaVon = c.Double(nullable: false),
                        GiaBan = c.Double(nullable: false),
                        CK = c.Double(nullable: false),
                        CK_Value = c.Double(nullable: false),
                        ThanhTien = c.Double(nullable: false),
                        GhiChu = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CtTiens",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaHDId = c.String(),
                        LoaiHD = c.String(),
                        NgayHoaDon = c.DateTime(nullable: false),
                        TKId = c.Int(),
                        TKDoiUngId = c.Int(),
                        PsNo = c.Double(nullable: false),
                        PsCo = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CtTons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaHDId = c.String(),
                        LoaiHD = c.String(),
                        NgayHoaDon = c.DateTime(nullable: false),
                        KhoId = c.Int(),
                        KhoDoiUngId = c.Int(),
                        SoLuongNhap = c.Double(nullable: false),
                        SoLuongXuat = c.Double(nullable: false),
                        MaHangId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DonViTinhs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HangHoas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RootId = c.String(),
                        MaHang = c.String(maxLength: 25, unicode: false),
                        TenHang = c.String(maxLength: 300),
                        SoLuong = c.Double(nullable: false),
                        NhomHangId = c.Int(nullable: false),
                        NhaSanXuat = c.String(maxLength: 250),
                        GiaMua = c.Double(nullable: false),
                        GiaBan = c.Double(nullable: false),
                        VAT = c.Int(nullable: false),
                        CheckVATMua = c.Boolean(nullable: false),
                        CheckVATBan = c.Boolean(nullable: false),
                        Status = c.Boolean(nullable: false),
                        CheckHome = c.Boolean(nullable: false),
                        GhiChu = c.String(),
                        ThumbnalImage = c.String(),
                        XuatXu = c.String(maxLength: 50),
                        DonViTinh = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.KhachHangGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        KhachHangId = c.Int(nullable: false),
                        GroupKHId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.KhachHangs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ma = c.String(nullable: false, maxLength: 25),
                        Name = c.String(nullable: false),
                        Phone = c.String(maxLength: 15),
                        DiaChi = c.String(maxLength: 300),
                        Email = c.String(),
                        NgaySinh = c.DateTime(),
                        GioiTinh = c.String(),
                        GhiChu = c.String(),
                        Status = c.Boolean(nullable: false),
                        MaSoThue = c.String(maxLength: 50),
                        NguoiLienHe = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NhaCungCaps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ma = c.String(nullable: false, maxLength: 25),
                        Name = c.String(nullable: false),
                        Phone = c.String(maxLength: 15),
                        DiaChi = c.String(maxLength: 300),
                        Email = c.String(),
                        NgaySinh = c.DateTime(),
                        GioiTinh = c.String(),
                        GhiChu = c.String(),
                        Status = c.Boolean(nullable: false),
                        MaSoThue = c.String(maxLength: 50),
                        NguoiLienHe = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NhaSanXuats",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NhomHangHoas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        NhomHangId = c.Int(),
                        Stt = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NhomKhachHangs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PhieuChis",
                c => new
                    {
                        MaPhieuChi = c.String(nullable: false, maxLength: 128),
                        NgayThu = c.DateTime(nullable: false),
                        NguoiThu = c.String(),
                        TongTien = c.Double(nullable: false),
                        PTThanhToan = c.String(),
                        HangMucChi = c.String(),
                        GhiChu = c.String(),
                        KhoId = c.Int(),
                        NVTN = c.Int(),
                        NCCId = c.Int(),
                    })
                .PrimaryKey(t => t.MaPhieuChi);
            
            CreateTable(
                "dbo.PhieuThus",
                c => new
                    {
                        MaPhieuThu = c.String(nullable: false, maxLength: 128),
                        NgayThu = c.DateTime(nullable: false),
                        NguoiThu = c.String(),
                        TongTien = c.Double(nullable: false),
                        PTThanhToan = c.String(),
                        HangMucThu = c.String(),
                        GhiChu = c.String(),
                        KhoId = c.Int(),
                        NVTN = c.Int(),
                        KhachHangID = c.Int(),
                    })
                .PrimaryKey(t => t.MaPhieuThu);
            
            CreateTable(
                "dbo.SoThuTus",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 128),
                        Pre = c.String(),
                        Nu = c.Int(nullable: false),
                        Stt = c.Int(nullable: false),
                        Sub = c.String(),
                    })
                .PrimaryKey(t => t.Name);
            
            CreateTable(
                "dbo.ThongTins",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Address = c.String(maxLength: 500),
                        Phone = c.String(maxLength: 15),
                        GhiChu = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ThuocTinhGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RootHHId = c.String(),
                        ThuocTinhId = c.Int(nullable: false),
                        Values = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ThuocTinhs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ThuocTinhs");
            DropTable("dbo.ThuocTinhGroups");
            DropTable("dbo.ThongTins");
            DropTable("dbo.SoThuTus");
            DropTable("dbo.PhieuThus");
            DropTable("dbo.PhieuChis");
            DropTable("dbo.NhomKhachHangs");
            DropTable("dbo.NhomHangHoas");
            DropTable("dbo.NhaSanXuats");
            DropTable("dbo.NhaCungCaps");
            DropTable("dbo.KhachHangs");
            DropTable("dbo.KhachHangGroups");
            DropTable("dbo.HangHoas");
            DropTable("dbo.DonViTinhs");
            DropTable("dbo.CtTons");
            DropTable("dbo.CtTiens");
            DropTable("dbo.CtBanHangs");
            DropTable("dbo.CauHinhHeThongs");
            DropTable("dbo.BanHangs");
        }
    }
}
