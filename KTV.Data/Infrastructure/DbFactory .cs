﻿using KTV.Data;
using KTV.Data.Infrastructure;

namespace PCDN.Data.Infrastructure
{
    public class DbFactory : Disposable,IDbFactory
    {
        private KTVDbContext dbContext;

        public KTVDbContext Init()
        {
            return dbContext ?? (dbContext = new KTVDbContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
