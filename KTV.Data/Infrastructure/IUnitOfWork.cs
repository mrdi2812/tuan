﻿
namespace KTV.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
