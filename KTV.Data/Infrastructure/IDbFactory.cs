﻿using System;

namespace KTV.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        KTVDbContext Init();
    }
}
