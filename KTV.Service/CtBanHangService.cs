﻿using KTV.Data;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class CtBanHangService
    {
        KTVDbContext context;
        public CtBanHangService()
        {
            context = new KTVDbContext();
        }
        public List<CtBanHang> GetAll()
        {
            return context.CtBanHangs.ToList();
        }

        public List<CtBanHang> GetByID(string id)
        {
            return context.CtBanHangs.Where(x => x.MaHD == id).ToList();
        }

        public CtBanHang Delete(CtBanHang model)
        {
            var query = context.CtBanHangs.Remove(model);
            context.SaveChanges();
            return query;
        }

        public CtBanHang Create(CtBanHang model)
        {
            var query = context.CtBanHangs.Add(model);
            context.SaveChanges();
            return query;
        }
        public IEnumerable<CtBanHang> GetListByMaHang(string maHH)
        {
            return context.CtBanHangs.Where(x => x.MaHang == maHH);
        }
    }
}
