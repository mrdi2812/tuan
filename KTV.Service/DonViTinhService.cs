﻿using KTV.Data;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class DonViTinhService
    {
        KTVDbContext context;
        public DonViTinhService()
        {
            context = new KTVDbContext();
        }
        public List<DonViTinh> GetAll()
        {
            return context.DonViTinhs.ToList();
        }
        public DonViTinh Add(DonViTinh model)
        {
            var query = context.DonViTinhs.Add(model);
            context.SaveChanges();
            return query;
        }
        public void Update(DonViTinh model)
        {
            var modelVm = context.DonViTinhs.Find(model.Id);
            context.Entry(modelVm).State = EntityState.Detached;
            context.Entry(model).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Delete(int id)
        {
            var query = context.DonViTinhs.FirstOrDefault(x => x.Id == id);
            context.DonViTinhs.Remove(query);
            context.SaveChanges();
        }
    }
}
