﻿using KTV.Data;
using KTV.Data.Infrastructure;
using KTV.Data.Repositories;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class HangHoaService
    {
        KTVDbContext context;
        public HangHoaService()
        {
            context = new KTVDbContext();
        }
        public List<HangHoa> GetAll()
        {
            return context.HangHoas.ToList();
        }     
        public IEnumerable<HangHoa> GetAllParent()
        {
            return context.HangHoas.Where(x =>x.RootId==null||x.MaHang==null);
        }
        public List<HangHoa> GetAllChild()
        {
            return context.HangHoas.Where(x => x.MaHang != null && x.RootId != null).ToList();
        }
        public List<HangHoa> GetListByRootId(string rootId)
        {
            return context.HangHoas.Where(x => x.RootId == rootId&&x.MaHang!=null).ToList();
        }
        public HangHoa Add(HangHoa model)
        {
            var query = context.HangHoas.Add(model);
            context.SaveChanges();
            return query;
        }
      
        public void Update(HangHoa model)
        {
            var modelVm = context.HangHoas.Find(model.ID);
            context.Entry(modelVm).State = EntityState.Detached;
            context.Entry(model).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Delete(int id)
        {
            var query = context.HangHoas.FirstOrDefault(x => x.ID == id);
            context.HangHoas.Remove(query);
            context.SaveChanges();
        }
        public int CheckMax()
        {
            var item = context.HangHoas.Where(x => x.MaHang != null).Max(x => x.ID);
            string chuoi = context.HangHoas.SingleOrDefault(x => x.ID == item).MaHang.Substring(2);
            return int.Parse(chuoi);
        }
        public void Save()
        {
            context.SaveChanges();
        }
        public HangHoa GetByMaHang(string maHang)
        {
            return context.HangHoas.SingleOrDefault(x => x.MaHang == maHang);
        }
        public HangHoa GetById(int id)
        {
            return context.HangHoas.SingleOrDefault(x => x.ID == id);
        }
        public HangHoa GetByRootId(string rootId)
        {
            return context.HangHoas.SingleOrDefault(x => x.RootId == rootId && x.MaHang == null);
        }
        public HangHoa GetByReadRootId(string rootId)
        {
            return context.HangHoas.FirstOrDefault(x => x.RootId == rootId && x.MaHang != null);
        }
        public IEnumerable<HangHoa> GetAllHangHoaMH()
        {
            return context.HangHoas.Where(x => x.MaHang != null);
        }

        public int CheckExitTenSP(string name)
        {
            var model = context.HangHoas.Where(x => x.TenHang.Contains(name));
            if (model != null)
                return model.Distinct().Count()+1;
            else
                return 1;
        }
        public bool CheckExit(string name)
        {
            var model = context.HangHoas.Where(x => x.TenHang==name).ToList();
            bool check = true;
            if (model.Count>0)
                check = false;
            return check;
        }
    }
}
