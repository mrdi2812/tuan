﻿using KTV.Data;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class PhieuThuService
    {
        KTVDbContext context;
        public PhieuThuService()
        {
            context = new KTVDbContext();
        }
        public List<PhieuThu> GetAll()
        {
            return context.PhieuThus.ToList();
        }
        public PhieuThu Add(PhieuThu model)
        {
            var query = context.PhieuThus.Add(model);
            context.SaveChanges();
            return query;
        }
        public void Update(PhieuThu model)
        {
            var modelVm = context.PhieuThus.Find(model.MaPhieuThu);
            context.Entry(modelVm).State = EntityState.Detached;
            context.Entry(model).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Delete(string id)
        {
            var query = context.PhieuThus.FirstOrDefault(x => x.MaPhieuThu == id);
            context.PhieuThus.Remove(query);
            context.SaveChanges();
        }
        public PhieuThu GetByID(string maPT)
        {
            return context.PhieuThus.FirstOrDefault(x => x.MaPhieuThu == maPT);
        }
    }
}
