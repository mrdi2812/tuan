﻿using KTV.Data;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class NCCService
    {
        KTVDbContext context;
        public NCCService()
        {
            context = new KTVDbContext();
        }
        public List<NhaCungCap> GetAll()
        {
            return context.NhaCungCaps.ToList();
        }
        public NhaCungCap Add(NhaCungCap model)
        {
            var query = context.NhaCungCaps.Add(model);
            context.SaveChanges();
            return query;
        }
        public void Update(NhaCungCap model)
        {
            var modelVm = context.NhaCungCaps.Find(model.Id);
            context.Entry(modelVm).State = EntityState.Detached;
            context.Entry(model).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Delete(int id)
        {
            var query = context.NhaCungCaps.FirstOrDefault(x => x.Id == id);
            context.NhaCungCaps.Remove(query);
            context.SaveChanges();
        }
    }
}
