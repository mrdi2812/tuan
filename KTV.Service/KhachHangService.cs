﻿using KTV.Common.Models;
using KTV.Data;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class KhachHangService
    {
        KTVDbContext context;
        public KhachHangService()
        {
            context = new KTVDbContext();
        }
        public List<KhachHang> GetAll()
        {
            return context.KhachHangs.ToList();
        }
        public IEnumerable<DSKH> GetAllKH()
        {
            var result = from kh in context.KhachHangs
            join pt in context.PhieuThus on kh.Id equals pt.KhachHangID
            into ps from pt in ps.DefaultIfEmpty()
            group new { kh,pt } by new {kh.Id,kh.Ma,kh.Name,kh.Phone} into d
            select new DSKH()
             {
                Id = d.Key.Id,
                Ma = d.Key.Ma,
                Name = d.Key.Name,
                Phone = d.Key.Phone,
                TongNo = 0,
                TienTraHang = 0
             };
            return result;
        }
        public KhachHang Add(KhachHang model)
        {
            var query = context.KhachHangs.Add(model);
            context.SaveChanges();
            return query;
        }
        public void Update(KhachHang model)
        {
            var modelVm = context.KhachHangs.Find(model.Id);
            context.Entry(modelVm).State = EntityState.Detached;
            context.Entry(model).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Delete(int id)
        {
            var query = context.KhachHangs.FirstOrDefault(x => x.Id == id);
            context.KhachHangs.Remove(query);
            context.SaveChanges();
        }

        public KhachHang GetById(int id)
        {
            return context.KhachHangs.FirstOrDefault(x => x.Id == id);
        }
    }
}
