﻿using KTV.Common.Models;
using KTV.Data;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class BanHangService
    {
        KTVDbContext context;
        public BanHangService()
        {
            context = new KTVDbContext();
        }
        public List<BanHang> GetAll()
        {
            return context.BanHangs.ToList();
        }

        public BanHang GetByID(string id)
        {
            return context.BanHangs.Where(x => x.MaHD == id).FirstOrDefault();
        }

        public BanHang Create(BanHang model)
        {
            var query = context.BanHangs.Add(model);
            context.SaveChanges();
            return query;
        }

        public BanHang Delete(BanHang model)
        {
            var query = context.BanHangs.Remove(model);
            context.SaveChanges();
            return context.BanHangs.Remove(model);
        }
        public IEnumerable<BanHang> GetByMaKH(int maKH)
        {
            return context.BanHangs.Where(x => x.KhachHangId == maKH);
        }

        public double TongTienByKH(int maKH)
        {
            return context.BanHangs.Where(x => x.KhachHangId == maKH).Select(x=>x.TongTien).Sum();
        }
        public double TongNoByKH(int maKH)
        {
            return context.BanHangs.Where(x => x.KhachHangId == maKH).Select(x => x.Congno).Sum();
        }
        public int TongDonByKH(int maKH)
        {
            return context.BanHangs.Where(x => x.KhachHangId == maKH).Count();
        }

        public IQueryable<DSHH> GetAllSPByKH(int maKH)
        {
            var query = from bh in context.BanHangs
                        join ct in context.CtBanHangs
                        on bh.MaHD equals ct.MaHD
                        join m in context.HangHoas
                        on ct.MaHang equals m.MaHang
                        where bh.KhachHangId == maKH
                        select new {bh,ct,m};
            var hh = query.GroupBy(x => x.ct.MaHang).Select(x=>new DSHH{
                MaHang = x.Key,
                SoLuong = x.Select(m=>m.ct.SoLuong).Sum(),
                NgayMua = x.Select(m=>m.bh.NgayBan).Max(),
                TenHang = x.Select(d=>d.m.TenHang).FirstOrDefault(),
                GiaBan = x.Select(d=>d.m.GiaBan).FirstOrDefault()
            });
            return hh;
        }
    }
}
