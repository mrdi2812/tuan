﻿using KTV.Data;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class NhaSXService
    {
        KTVDbContext context;
        public NhaSXService()
        {
            context = new KTVDbContext();
        }
        public List<NhaSanXuat> GetAll()
        {
            return context.NhaSanXuats.ToList();
        }
        public NhaSanXuat Add(NhaSanXuat model)
        {
            var query =  context.NhaSanXuats.Add(model);
            context.SaveChanges();
            return query;
        }
        public void Update(NhaSanXuat model)
        {
            var modelVm = context.NhaSanXuats.Find(model.Id);
            context.Entry(modelVm).State = EntityState.Detached;
            context.Entry(model).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Delete(int id)
        {
            var query = context.NhaSanXuats.FirstOrDefault(x => x.Id == id);
            context.NhaSanXuats.Remove(query);
            context.SaveChanges();
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
