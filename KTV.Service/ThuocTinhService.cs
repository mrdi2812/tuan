﻿using KTV.Data;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class ThuocTinhService
    {
        KTVDbContext context;
        public ThuocTinhService()
        {
            context = new KTVDbContext();
        }
        public List<ThuocTinh> GetAll()
        {
            return context.ThuocTinhs.ToList();
        }
        public ThuocTinh Add(ThuocTinh model)
        {
            var query = context.ThuocTinhs.Add(model);
            context.SaveChanges();
            return query;
        }
        public void Update(ThuocTinh model)
        {
            var modelVm = context.ThuocTinhs.Find(model.Id);
            context.Entry(modelVm).State = EntityState.Detached;
            context.Entry(model).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Delete(int id)
        {
            var query = context.ThuocTinhs.FirstOrDefault(x => x.Id == id);
            context.ThuocTinhs.Remove(query);
            context.SaveChanges();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public ThuocTinh GetById(int id)
        {
            return context.ThuocTinhs.SingleOrDefault(x => x.Id == id);
        }
        public ThuocTinh GetByName(string name)
        {
            return context.ThuocTinhs.FirstOrDefault(x => x.Name.Contains(name));
        }
    }
}
