﻿using KTV.Common;
using KTV.Data;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class ThuocTinhGroupService
    {
        KTVDbContext context;
        public ThuocTinhGroupService()
        {
            context = new KTVDbContext();
        }
        public List<ThuocTinhGroup> GetByRootId(string rootId)
        {
            return context.ThuocTinhGroups.Where(x => x.RootHHId == rootId).ToList();
        }
        public void AddThuocTinhByProduct(List<ListThuocTinh> thuocTinhs,string rootId)
        {
            var model = context.ThuocTinhGroups.Where(x => x.RootHHId == rootId).ToList();
            foreach(var item in model)
            {
                context.ThuocTinhGroups.Remove(item);
                context.SaveChanges();
            }            
            for (int i = 0; i < thuocTinhs.Count; i++)
            {
                ThuocTinhGroup item = new ThuocTinhGroup();
                item.RootHHId = rootId;
                item.ThuocTinhId = thuocTinhs[i].ID;
                item.Values = thuocTinhs[i].Values;
                context.ThuocTinhGroups.Add(item);
                context.SaveChanges();
            }
        }
        public int CountThuocTinhByRootId(string rootId)
        {
            return context.ThuocTinhGroups.Where(x => x.RootHHId == rootId).Count();
        }   
    }
}
