﻿using KTV.Data;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class NhomHHService
    {
        KTVDbContext context;
        public NhomHHService()
        {
            context = new KTVDbContext();
        }
        public List<NhomHangHoa> GetAll()
        {
            return context.NhomHangHoas.ToList();
        }
        public NhomHangHoa Add(NhomHangHoa model)
        {
            var query = context.NhomHangHoas.Add(model);
            context.SaveChanges();
            return query;
        }
        public void Update(NhomHangHoa model)
        {
            context.NhomHangHoas.Attach(model);
            context.SaveChanges();
        }
        public void Delete(int id)
        {
            var query = context.NhomHangHoas.FirstOrDefault(x => x.Id == id);
            context.NhomHangHoas.Remove(query);
            context.SaveChanges();
        }

        public NhomHangHoa GetById(int id)
        {
            return context.NhomHangHoas.SingleOrDefault(x => x.Id == id);
        }
        public NhomHangHoa GetByName(string name)
        {
            return context.NhomHangHoas.SingleOrDefault(x => x.Name == name);
        }

        public bool CheckExit(string name)
        {
            var model = context.NhomHangHoas.Where(x => x.Name.Contains(name)).ToList();
            if (model.Count>0)
                return false;
            else
                return true;
        }
    }
}
