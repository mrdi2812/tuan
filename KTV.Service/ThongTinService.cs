﻿using KTV.Data;
using KTV.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace KTV.Service
{
    public class ThongTinService
    {
        KTVDbContext context;
        public ThongTinService()
        {
            context = new KTVDbContext();
        }
        public List<ThongTin> GetAll()
        {
            return context.ThongTins.ToList();
        }
        public ThongTin Add(ThongTin model)
        {
            var query = context.ThongTins.Add(model);
            context.SaveChanges();
            return query;
        }
        public void Update(ThongTin model)
        {
            var modelVm = context.DonViTinhs.Find(model.ID);
            context.Entry(modelVm).State = EntityState.Detached;
            context.Entry(model).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Delete(int id)
        {
            var query = context.ThongTins.FirstOrDefault(x => x.ID == id);
            context.ThongTins.Remove(query);
            context.SaveChanges();
        }
        public ThongTin GetDefault()
        {
            return context.ThongTins.FirstOrDefault();
        }
    }
}
